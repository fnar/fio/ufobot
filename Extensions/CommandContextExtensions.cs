﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using DSharpPlus.CommandsNext;
using DSharpPlus.Entities;
using DSharpPlus.Interactivity.Extensions;
using Microsoft.Extensions.Configuration;

namespace UFOBot.Extensions
{
    public static class CommandContextExtensions
    {
        private static ConfigurationSection discordConfig = (ConfigurationSection)Globals.configuration.GetSection("Discord");

        public static async Task<DiscordMember> GetMemberAsync(this CommandContext ctx)
        {
            ConfigurationSection discordConfig = (ConfigurationSection)Globals.configuration.GetSection("Discord");
            if (ctx.Guild == null)
            {
                ulong mgid = discordConfig.GetValue<ulong>("ManagedGuildID");
                DiscordGuild guild = ctx.Client.Guilds.Select(x => x.Value).Where(x => x.Id == mgid).First();
                DiscordMember member = await guild.GetMemberAsync(ctx.User.Id);
                return member;
            }
            else
            {
                return ctx.Member;
            }
        }

        public static bool IsDM(this CommandContext ctx)
        {
            return ctx.Guild == null;
        }

        public static async Task<DiscordGuild> GetGuildAsync(this CommandContext ctx)
        {
            if (ctx.Guild == null)
            {
                ulong mgid = discordConfig.GetValue<ulong>("ManagedGuildID");
                DiscordGuild guild = ctx.Client.Guilds.Select(x => x.Value).Where(x => x.Id == mgid).First();
                if (guild == null)
                {
                    await ctx.RespondAsync("[BOT CONFIGURATION ERROR] Guild not properly set up.");
                }
                return guild;
            }
            else
            {
                return ctx.Guild;
            }
        }

        public static async Task ConditionalWarnUserNicknameConvention(this CommandContext ctx)
        {
            var member = await ctx.GetMemberAsync();
            if (!member.NicknameMatchesConvention())
            {
                await ctx.SendDMResponse(Commands.CommandConstants.NicknameConventionWarning);
            }
        }

        public static async Task<DiscordChannel> GetAuditLogChannelAsync(this CommandContext ctx)
        {
            DiscordGuild guild = await ctx.GetGuildAsync();
            return guild.GetAuditLogChannel();
        }

        public static async Task<DiscordMessage> SendDMResponseWithOriginalCommand(this CommandContext ctx) => await SendDMResponseWithOriginalCommand(ctx, null, null);
        public static async Task<DiscordMessage> SendDMResponseWithOriginalCommand(this CommandContext ctx, string responseMessage) => await SendDMResponseWithOriginalCommand(ctx, responseMessage, null);
        public static async Task<DiscordMessage> SendDMResponseWithOriginalCommand(this CommandContext ctx, DiscordEmbed embed) => await SendDMResponseWithOriginalCommand(ctx, null, embed);

        private static async Task<DiscordMessage> SendDMResponseWithOriginalCommand(CommandContext ctx, string responseMessage = null, DiscordEmbed embed = null)
        {
            string message = ctx.Message.GetFormattedOriginalRequest();
            if (!string.IsNullOrEmpty(responseMessage))
            {
                // Normal plaintext
                message += "\n" + responseMessage;
            }

            // Send the original message back
            DiscordMember member = await ctx.GetMemberAsync();
            try
            {
                if (embed != null)
                {
                    return await member.SendMessageAsync(embed);
                }
                else
                {
                    return await member.SendMessageAsync(message);
                }

            }
            catch (DSharpPlus.Exceptions.UnauthorizedException)
            {
                // Soft-fail, since this means they don't want DMs
                return null;
            }
        }

        public static async Task<DiscordMessage> SendDMResponse(this CommandContext ctx, string msg)
        {
            var member = await ctx.GetMemberAsync();
            return await member.SendDM(msg, true);
        }

        static List<string> AbortTextOptions = new List<string>
        {
            "ABORT",
            "CANCEL",
            "QUIT",
            "HELP"
        };

        public static async Task<DiscordMessage> GetNextMessageAsyncAndHandleAbort(this CommandContext ctx, TimeSpan? timeoutOverride = null)
        {
            var member = await ctx.GetMemberAsync();

            var response = await ctx.Message.GetNextMessageAsync(timeoutOverride: timeoutOverride);
            if (response.TimedOut)
            {
                await member.SendMessageAsync("Command timed out. Aborting.");
                return null;
            }

            var responseText = response.Result.Content.Trim().ToUpper();
            if (AbortTextOptions.Contains(responseText) || responseText.StartsWith("!"))
            {
                await member.SendMessageAsync("Aborting.");
                return null;
            }

            return response.Result;
        }
    }
}
