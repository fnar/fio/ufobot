﻿using System;
using System.Linq;

using DSharpPlus;
using DSharpPlus.Entities;
using Microsoft.Extensions.Configuration;

namespace UFOBot.Extensions
{
    public static class DiscordClientExtensions
    {
        private static ConfigurationSection discordConfig = (ConfigurationSection)Globals.configuration.GetSection("Discord");

        public static DiscordGuild GetGuild(this DiscordClient client)
        {
            ulong mgid = discordConfig.GetValue<ulong>("ManagedGuildID");
            DiscordGuild guild = client.Guilds.Select(x => x.Value).Where(x => x.Id == mgid).First();
            if (guild == null)
            {
                throw new InvalidOperationException("Guild not properly set up.");
            }

            return guild;
        }
    }
}
