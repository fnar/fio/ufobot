﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using DSharpPlus.Entities;
using Microsoft.Extensions.Configuration;
using Serilog;
using UFOBot.Database;

namespace UFOBot.Extensions
{
    public static class DiscordMemberExtensions
    {
        private static ConfigurationSection discordConfig = (ConfigurationSection)Globals.configuration.GetSection("Discord");

        public static bool IsModerator(this DiscordMember member)
        {
            List<DiscordRole> baseRoles = member.Guild.Roles
                .Where(x => x.Value.Name.ToUpper() == discordConfig.GetValue<string>("ModeratorBaseRole").ToUpper())
                .Select(x => x.Value)
                .ToList();
            if (baseRoles.Count != 1)
            {
                Log.Error($"Unable to find Moderator role: {discordConfig.GetValue<string>("ModeratorBaseRole")}");
                return false;
            }

            return member.Roles.Contains(baseRoles[0]);
        }

        public static bool IsModerating(this DiscordMember member)
        {
            List<DiscordRole> roles = member.Guild.Roles
                .Where(x => x.Value.Name.ToUpper() == discordConfig.GetValue<string>("ModeratorPromotedRole").ToUpper())
                .Select(x => x.Value)
                .ToList();
            if (roles.Count != 1)
            {
                Log.Error($"Unable to find Moderator role: {discordConfig.GetValue<string>("ModeratorBaseRole")}");
                return false;
            }

            return member.Roles.Contains(roles[0]);
        }

        public static async Task<bool> GrantModerating(this DiscordMember member)
        {
            if (!member.IsModerator())
            {
                return false;
            }

            List<DiscordRole> roles = member.Guild.Roles
                .Where(x => x.Value.Name.ToUpper() == discordConfig.GetValue<string>("ModeratorPromotedRole").ToUpper())
                .Select(x => x.Value)
                .ToList();
            if (roles.Count != 1)
            {
                Log.Error($"Unable to find Moderator role: {discordConfig.GetValue<string>("ModeratorBaseRole")}");
                return false;
            }

            await member.GrantRoleAsync(roles[0], $"Enabling Moderating mode on {member.DisplayName}");
            return true;
        }

        public static async Task<bool> UngrantModerating(this DiscordMember member)
        {
            if (!member.IsModerating())
            {
                return false;
            }

            List<DiscordRole> roles = member.Guild.Roles
                .Where(x => x.Value.Name.ToUpper() == discordConfig.GetValue<string>("ModeratorPromotedRole").ToUpper())
                .Select(x => x.Value)
                .ToList();
            if (roles.Count != 1)
            {
                Log.Error($"Unable to find Moderator role: {discordConfig.GetValue<string>("ModeratorBaseRole")}");
                return false;
            }

            if (member.Roles.Contains(roles[0]))
            {
                await member.RevokeRoleAsync(roles[0], $"Disabling Moderating mode on {member.DisplayName}");
                return true;
            }

            return false;
        }

        public static string GetInGameUsername(this DiscordMember member)
        {
            using (var DB = new UFODbContext())
            {
                var model = DB.InGameUserToDiscordUserModels.FirstOrDefault(e => e.DiscordId == member.Id);
                if (model != null)
                {
                    return model.InGameUsername;
                }

                return null;
            }
        }

        public static bool IsGovernor(this DiscordMember member)
        {
            return member.Roles.Any(r => r.Name.StartsWith("Governor"));
        }

        public static async Task<List<DiscordChannel>> GetGovernorChannelAccess(this DiscordMember member)
        {
            List<DiscordChannel> channels = new List<DiscordChannel>();
            if (member.IsGovernor())
            {
                var inGameUsername = member.GetInGameUsername();
                if (inGameUsername != null)
                {
                    var settledPlanetDefs = await Globals.DataRetrieval.GetSettledPlanetDefs();
                    if (settledPlanetDefs == null)
                    {
                        // Bail
                        return channels;
                    }

                    var planetDef = settledPlanetDefs.Where(spd => spd.GovernorUsername != null).FirstOrDefault(spd => spd.GovernorUsername.ToUpper() == inGameUsername.ToUpper());
                    if (planetDef != null)
                    {
                        var textChannelPlanetNamePrefix = planetDef.Name.Replace(" ", "-").ToLower() + "-";
                        channels = member.Guild.Channels.Where(ch => ch.Value.Name.StartsWith(textChannelPlanetNamePrefix)).Select(ch => ch.Value).ToList();
                    }
                }
            }

            return channels;
        }

        public static async Task<DiscordChannel> GetGovernorPoliticsChannel(this DiscordMember member)
        {
            return (await member.GetGovernorChannelAccess()).FirstOrDefault(dc => dc.Name.ToLower().EndsWith("-politics"));
        }

        public static async Task GrantGovernor(this DiscordMember member)
        {
            var guild = member.Guild;
            var inGameUsername = member.GetInGameUsername();
            var defaultGovernorRole = await guild.GetDefaultGovernorRole();

            var settledPlanetDefs = await Globals.DataRetrieval.GetSettledPlanetDefs();
            if (settledPlanetDefs != null)
            {
                var planetDef = settledPlanetDefs.Where(spd => spd.GovernorUsername != null).FirstOrDefault(spd => spd.GovernorUsername.ToUpper() == inGameUsername.ToUpper());
                if (planetDef != null)
                {
                    var governorRoleText = $"Governor: {planetDef.Name}";

                    // First, search for an existing role
                    var governorRole = guild.Roles.Values.FirstOrDefault(r => r.Name == governorRoleText);
                    if (governorRole != null)
                    {
                        await member.GrantRoleAsync(governorRole);
                        await Task.Delay(250);
                    }
                    else
                    {
                        try
                        {
                            // Try and create the role
                            var governorRoleColor = new DiscordColor(discordConfig.GetSection("GovernorColor").Get<string>());
                            governorRole = await guild.CreateRoleAsync(governorRoleText, permissions: null, color: governorRoleColor, hoist: false, mentionable: false, reason: "UFOBot created");
                            await Task.Delay(500);
                            await governorRole.ModifyPositionAsync(defaultGovernorRole.Position - 1);
                            await Task.Delay(500);
                        }
                        catch
                        {
                            // Fall through
                        }

                        await member.GrantRoleAsync(defaultGovernorRole);
                        await Task.Delay(1000);

                        if (governorRole != null)
                        {
                            await member.GrantRoleAsync(governorRole);
                            await Task.Delay(1000);
                        }
                    }
                }
            }
        }

        public static async Task UngrantGovernor(this DiscordMember member)
        {
            var governorRoles = member.Roles.Where(r => r.Name.StartsWith("Governor"));
            foreach (var governorRole in governorRoles)
            {
                await member.RevokeRoleAsync(governorRole);
                await Task.Delay(250);
            }
        }

        public static bool IsVerified(this DiscordMember member)
        {
            var VerifiedRole = member.Guild.GetVerifiedRole();
            return member.Roles.Any(r => r == VerifiedRole);
        }

        private const string NicknameConventionRegexPattern =
            @"^(\[" +                          // Opening bracket (start capture)
            @"(?<Corporation>[A-Z0-9]{1,4})" + // CorpCode 1-4 uppercase letters/numbers
            @"\]\s?)?" +                       // Closing bracket (close capture and mark it optional). Space afterwards (optional)
            @"(?<Username>[^\s]+?)" +          // Username, any character exception space
            @"\s?\|\s?" +                      // Separator: Space (optional) | Space (optional)
            @"(?<CompanyCode>[A-Z0-9]{1,4})" + // CompanyCode 1-4 characters
            @"((\s|\()" +                      // Space or open parenthesis
            @"(.*))?$";                        // Any other characters (optional)
        private static Regex NicknameConventionRegex = new Regex(NicknameConventionRegexPattern, RegexOptions.Compiled);

        public static bool NicknameMatchesConvention(this DiscordMember member)
        {
            return member.DisplayName.StringMatchesConvention();
        }

        public static bool GetNicknameConventionParts(this DiscordMember member, out string Corporation, out string Username, out string CompanyCode)
        {
            return member.DisplayName.GetNicknameConventionParts(out Corporation, out Username, out CompanyCode);
        }

        public static bool GetNicknameConventionParts(this string Nickname, out string Corporation, out string Username, out string CompanyCode)
        {
            Corporation = null;
            Username = null;
            CompanyCode = null;

            if (Nickname != null)
            {
                Match m = NicknameConventionRegex.Match(Nickname);
                if (m != null)
                {
                    Corporation = m.Groups["Corporation"].Value;
                    Username = m.Groups["Username"].Value;
                    CompanyCode = m.Groups["CompanyCode"].Value;

                    return !string.IsNullOrWhiteSpace(Username) && !string.IsNullOrWhiteSpace(CompanyCode);
                }
            }

            return false;
        }

        public static bool StringMatchesConvention(this string Nickname)
        {
            if (Nickname != null)
            {
                return NicknameConventionRegex.IsMatch(Nickname);
            }

            return false;
        }

        public static async Task<DiscordMessage> SendDM(this DiscordMember member, string message, bool bFallbackToAccessRequest = false)
        {
            try
            {
                var dm = await member.SendMessageAsync(message);
                return dm;
            }
            catch (DSharpPlus.Exceptions.UnauthorizedException)
            {
                if (bFallbackToAccessRequest)
                {
                    var accessRequestChannel = member.Guild.GetAccessRequestChannel();
                    return await accessRequestChannel.SendMessageAsync($"{member.Mention} {message}");
                }

                return null;
            }
        }
    }
}
