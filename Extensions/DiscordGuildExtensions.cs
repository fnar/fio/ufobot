﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using DSharpPlus.Entities;
using Microsoft.Extensions.Configuration;

namespace UFOBot.Extensions
{
    public static class DiscordGuildExtensions
    {
        private static ConfigurationSection discordConfig = (ConfigurationSection)Globals.configuration.GetSection("Discord");

        private const string planetRegexPattern = @"(\b\w\w\-\d\d\d\w\b)";
        private const string factionRegexPattern = @"([Aa][Rr][Ee][Aa]$)";

        private static Regex planetRegex = new Regex(planetRegexPattern, RegexOptions.Compiled);
        private static Regex factionRegex = new Regex(factionRegexPattern, RegexOptions.Compiled);

        private static List<DiscordChannel> GetEligibleCategories(this DiscordGuild guild)
        {
            string[] ignoredCategories = discordConfig.GetSection("IgnoredCategories").Get<string[]>();
            return guild.Channels.Values.Where(channel => channel.Parent == null && ignoredCategories.Contains(channel.Name.ToUpper()) == false).ToList();
        }

        public static List<DiscordChannel> GetPlanetCategories(this DiscordGuild guild)
        {
            return guild.GetEligibleCategories()
                .Where(channel => planetRegex.IsMatch(channel.Name))
                .OrderBy(channel => channel.Name)
                .ToList();
        }

        public static List<DiscordChannel> GetFactionCategories(this DiscordGuild guild)
        {
            return guild.GetEligibleCategories()
                .Where(channel => factionRegex.IsMatch(channel.Name))
                .OrderBy(channel => channel.Name)
                .ToList();
        }

        public static List<DiscordChannel> GetGeneralCategories(this DiscordGuild guild)
        {
            return guild.GetEligibleCategories()
                .Where(channel => planetRegex.IsMatch(channel.Name) == false && factionRegex.IsMatch(channel.Name) == false)
                .OrderBy(channel => channel.Name)
                .ToList();
        }

        public static DiscordChannel GetAuditLogChannel(this DiscordGuild guild)
        {
            string auditLogChannelName = discordConfig.GetSection("AuditLogChannelName").Get<string>();
            DiscordChannel channel = guild.Channels.Where(p => p.Value.Name.ToUpper() == auditLogChannelName).Select(c => c.Value).FirstOrDefault();
            return channel;
        }

        public static DiscordChannel GetAccessRequestChannel(this DiscordGuild guild)
        {
            const string accessRequestChannelName = "ACCESS-REQUEST";
            DiscordChannel channel = guild.Channels.Where(p => p.Value.Name.ToUpper() == accessRequestChannelName).Select(c => c.Value).FirstOrDefault();
            return channel;
        }

        public static async Task<DiscordRole> GetDefaultGovernorRole(this DiscordGuild guild)
        {
            string defaultGovernorRoleName = discordConfig.GetSection("DefaultGovernorRole").Get<string>();
            string governorColorStr = discordConfig.GetSection("GovernorColor").Get<string>();
            if (guild.Roles.Any(r => r.Value.Name == defaultGovernorRoleName))
            {
                return guild.Roles.First(r => r.Value.Name == defaultGovernorRoleName).Value;
            }
            else
            {
                var moderatorBaseRoleName = discordConfig.GetValue<string>("ModeratorBaseRole").ToUpper();
                var moderatorBaseRole = guild.Roles.Values.FirstOrDefault(r => r.Name.ToUpper() == moderatorBaseRoleName);
                if (moderatorBaseRole == null)
                {
                    throw new InvalidOperationException("Moderator Base Role undefined");
                }

                var governorColor = new DiscordColor(governorColorStr);
                var defaultGovernorRole = await guild.CreateRoleAsync(defaultGovernorRoleName, permissions: null, color: governorColor, hoist: true, mentionable: false, reason: "UFOBot created");
                await Task.Delay(250);
                await defaultGovernorRole.ModifyPositionAsync(moderatorBaseRole.Position - 1);
                await Task.Delay(250);
                return defaultGovernorRole;
            }
        }

        public static DiscordRole GetVerifiedRole(this DiscordGuild guild)
        {
            return guild.Roles
                .Select(x => x.Value)
                .Where(role => role.Name.ToUpper() == "VERIFIED")
                .FirstOrDefault();
        }

        public static async Task EmitMessagesToAuditLog(this DiscordGuild guild, string header, List<string> messages)
        {
            if (messages.Count > 0)
            {
                var discordConfig = (ConfigurationSection)Globals.configuration.GetSection("Discord");
                string auditLogChannelName = discordConfig.GetSection("AuditLogChannelName").Get<string>();
                DiscordChannel auditLogChannel = guild.Channels.Where(p => p.Value.Name.ToUpper() == auditLogChannelName).Select(c => c.Value).FirstOrDefault();

                StringBuilder sb = new StringBuilder();
                sb.AppendLine(header);
                sb.AppendLine("```");
                foreach (var message in messages)
                {
                    if ((sb.Length + message.Length) > 1900)
                    {
                        // Send it
                        sb.AppendLine("```");

                        await auditLogChannel.SendMessageAsync(sb.ToString());

                        sb.Clear();
                    }

                    if (sb.Length == 0)
                    {
                        sb.AppendLine("```");
                    }

                    sb.AppendLine(message);
                }

                if (sb.Length > 0)
                {
                    sb.AppendLine("```");
                    await auditLogChannel.SendMessageAsync(sb.ToString());
                }
            }
        }

        public static async Task<Tuple<bool, DiscordChannel>> GuardedCreateChannelAsync(this DiscordGuild guild, string name, DSharpPlus.ChannelType type, DiscordChannel parent = null, string reason = null)
        {
            //var channels = await guild.GetChannelsAsync();
            if (guild.Channels.Count == 500)
            {
                Console.Error.WriteLine($"Failed to create channel {name}. At maximum channel count.");
                return new Tuple<bool, DiscordChannel>(false, null);
            }

            try
            {
                var channel = await guild.CreateChannelAsync(name: name, type: type, parent: parent, reason: reason);
                return new Tuple<bool, DiscordChannel>(true, channel);
            }
            catch
            {
                return new Tuple<bool, DiscordChannel>(false, null);
            }
        }
    }
}
