﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using DSharpPlus.Entities;
using Microsoft.Extensions.Configuration;

namespace UFOBot.Extensions
{
    public static class DiscordChannelExtensions
    {
        private static ConfigurationSection discordConfig = (ConfigurationSection)Globals.configuration.GetSection("Discord");

        public static bool IsManagedChannel(this DiscordChannel chan)
        {
            if (chan == null)
                return false;

            string[] allowedChannels = discordConfig.GetSection("ManageChannelNames").Get<string[]>();
            return allowedChannels.Contains(chan.Name.ToUpper());
        }

        public static bool IsInIgnoredCategory(this DiscordChannel chan)
        {
            if (chan == null)
            {
                return false;
            }

            string[] igoredCategories = discordConfig.GetSection("IgnoredCategories").Get<string[]>();
            return igoredCategories.Contains(chan.Parent.Name.ToUpper());
        }

        const string categoryRegexPattern = @"((?<PlanetName>.+)(\s\|\s))?(?<PlanetNaturalId>\w{2}\-\d{3}[A-Za-z])";
        private static Regex categoryRegex = new Regex(categoryRegexPattern, RegexOptions.Compiled | RegexOptions.IgnoreCase);
        public static void GetPlanetNameAndId(this DiscordChannel chan, out string planetNaturalId, out string planetName)
        {
            planetNaturalId = null;
            planetName = null;

            if (chan == null)
                return;

            DiscordChannel channelToCheck = (chan.Parent != null) ? chan.Parent : chan;
            var mc = categoryRegex.Matches(channelToCheck.Name);
            if (mc.Count == 1)
            {
                planetNaturalId = mc[0].Groups["PlanetNaturalId"].Value;
                planetName = mc[0].Groups["PlanetName"].Value;
                if (string.IsNullOrEmpty(planetName))
                {
                    planetName = planetNaturalId;
                }
            }
        }

        private const int MaxMessageLength = 2000;
        public static async Task SendMessageLinesAsync(this DiscordChannel chan, List<string> lines)
        {
            List<string> messages = new List<string>();

            var sb = new StringBuilder();
            foreach (var line in lines)
            {
                if (sb.Length + line.Length > MaxMessageLength)
                {
                    messages.Append(sb.ToString());
                    sb = new StringBuilder();
                }

                sb.AppendLine(line);
            }

            messages.Add(sb.ToString());

            foreach (var message in messages)
            {
                await chan.SendMessageAsync(message);
                await Task.Delay(250);
            }
        }
    }
}
