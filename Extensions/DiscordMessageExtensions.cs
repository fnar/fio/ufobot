﻿using DSharpPlus.Entities;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace UFOBot.Extensions
{
    public static class DiscordMessageExtensions
    {
        private static ConfigurationSection discordConfig = (ConfigurationSection)Globals.configuration.GetSection("Discord");

        public static string GetFormattedOriginalRequest(this DiscordMessage message)
        {
            const int messageCutOffLength = 64;

            return
                "Your request: \n" +
                "> " + (message.Content.Length > messageCutOffLength ? message.Content.Substring(0, messageCutOffLength) + "..." : message.Content) + "\n\n";
        }

        public static bool IsDM(this DiscordMessage message)
        {
            return message.Channel.Guild == null;
        }
    }
}
