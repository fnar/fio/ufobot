﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using Serilog;

using UFOBot.Extensions;

namespace UFOBot.Commands
{
    public class FactionCommands : BaseCommandModule
    {
        [Command("clear-all-factions")]
        [Description("Remove yourself from associations with all faction roles.\n")]
        public async Task ClearAllFactionsCommand(CommandContext ctx)
        {
            DiscordGuild guild = await ctx.GetGuildAsync();
            DiscordMember member = await ctx.GetMemberAsync();
            if (member == null)
            {
                await ctx.RespondAsync("Unable to identify you as a member in the guild.");
                return;
            }

            List<DiscordRole> memberFactionRoles = member.Roles
                .Where(role => role.Name.ToUpper().StartsWith("FACTION:"))
                .ToList();
            // Make sure we're not trying to replace it with the same role.  That's just extra work.
            if (memberFactionRoles.Count > 0)
            {
                foreach (DiscordRole role in memberFactionRoles)
                {
                    await member.RevokeRoleAsync(role, "Faction change");
                }
            }

            if (!ctx.IsDM() && !ctx.Channel.IsManagedChannel())
            {
                var dm = await ctx.SendDMResponseWithOriginalCommand();
                if (dm != null)
                {
                    await dm.CreateReactionAsync(DiscordEmoji.FromName(ctx.Client, CommandConstants.SuccessReaction));
                }

                // Delete the original message
                await ctx.Message.DeleteAsync();
            }
            else
            {
                await ctx.Message.CreateReactionAsync(DiscordEmoji.FromName(ctx.Client, CommandConstants.SuccessReaction));
            }

            await ctx.ConditionalWarnUserNicknameConvention();
        }

        [Command("clear-faction")]
        [Description("Remove yourself from associations with a faction.\n")]
        public async Task ClearFactionCommand(CommandContext ctx,
            [Description("Faction name")]
            [RemainingText]
            string name)
        {
            if (name == null)
            {
                // Due to "[RemainingText]" it's considered optional
                return;
            }

            DiscordGuild guild = await ctx.GetGuildAsync();
            DiscordMember member = await ctx.GetMemberAsync();
            if (member == null)
            {
                await ctx.RespondAsync("Unable to identify you as a member in the guild.");
                return;
            }

            List<DiscordRole> memberFactionRoles = member.Roles
                .Where(role => role.Name.ToUpper().StartsWith("FACTION:"))
                .Where(role => role.Name.ToUpper().Contains(name.ToUpper()))
                .ToList();

            DiscordEmoji reaction = null;
            if (memberFactionRoles.Count == 1)
            {
                await member.RevokeRoleAsync(memberFactionRoles[0], "Faction change");
                reaction = DiscordEmoji.FromName(ctx.Client, CommandConstants.SuccessReaction);
            }
            else
            {
                reaction = DiscordEmoji.FromName(ctx.Client, CommandConstants.FailureReaction);
            }

            if (!ctx.IsDM() && !ctx.Channel.IsManagedChannel())
            {
                // Send new DM response
                var dm = await ctx.SendDMResponseWithOriginalCommand();
                if (dm != null)
                {
                    await dm.CreateReactionAsync(reaction);
                }

                // Delete the original message
                await ctx.Message.DeleteAsync();
            }
            else
            {
                await ctx.Message.CreateReactionAsync(reaction);
            }

            await ctx.ConditionalWarnUserNicknameConvention();
        }


        [Command("faction")]
        [Description("Associate yourself (assign role) with a particular faction.")]
        public async Task FactionCommand(CommandContext ctx,
            [Description("Faction name")]
            [RemainingText]
            string name)
        {
            if(name == null)
            {
                // Due to "[RemainingText]" it's considered optional
                return;
            }

            DiscordGuild guild = await ctx.GetGuildAsync();
            DiscordMember member = await ctx.GetMemberAsync();
            if (member == null)
            {
                await ctx.RespondAsync("Unable to identify you as a member in the guild.");
                return;
            }

            List<DiscordRole> roles = guild.Roles
                .Select(x => x.Value)
                .Where(role => role.Name.ToUpper().StartsWith("FACTION:"))
                .Where(role => role.Name.ToUpper().Contains(name.ToUpper()))
                .ToList();
            Log.Debug($"Found {roles.Count} results for {name}");

            DiscordEmoji reaction = null;
            if(roles.Count == 1)
            {
                await member.GrantRoleAsync(roles[0], "Faction change");
                reaction = DiscordEmoji.FromName(ctx.Client, CommandConstants.SuccessReaction);
            }
            else
            {
                reaction = DiscordEmoji.FromName(ctx.Client, CommandConstants.FailureReaction);
            }

            if (!ctx.IsDM() && !ctx.Channel.IsManagedChannel())
            {
                // Send new DM response
                var dm = await ctx.SendDMResponseWithOriginalCommand();
                if (dm != null)
                {
                    await dm.CreateReactionAsync(reaction);
                }

                // Delete the original message
                await ctx.Message.DeleteAsync();
            }
            else
            {
                await ctx.Message.CreateReactionAsync(reaction);
            }

            await ctx.ConditionalWarnUserNicknameConvention();
        }

        [Command("factions")]
        [Description("List factions.")]
        public async Task FactionListCommand(CommandContext ctx)
        {
            DiscordGuild guild = await ctx.GetGuildAsync();
            DiscordMember member = await ctx.GetMemberAsync();
            if (member == null)
            {
                await ctx.RespondAsync("Unable to identify you as a member in the guild.");
                return;
            }

            List<DiscordRole> roles = guild.Roles
                .Select(x => x.Value)
                .Where(role => role.Name.ToUpper().StartsWith("FACTION:"))
                .ToList();

            string responseMsg = null;
            DiscordEmoji reaction = null;
            if (roles.Count > 0)
            {
                responseMsg = "Available factions:\n" + String.Join("\n", roles.Select(r => $"> {r.Name}"));
                reaction = DiscordEmoji.FromName(ctx.Client, CommandConstants.SuccessReaction);
            }
            else
            {
                responseMsg = "No faction roles found!";
                reaction = DiscordEmoji.FromName(ctx.Client, CommandConstants.FailureReaction);
            }

            if (!ctx.IsDM() && !ctx.Channel.IsManagedChannel())
            {
                // Send new DM response
                var dm = await ctx.SendDMResponseWithOriginalCommand(responseMsg);
                if (dm != null)
                {
                    await dm.CreateReactionAsync(reaction);
                }

                // Delete the original message
                await ctx.Message.DeleteAsync();
            }
            else
            {
                await ctx.Message.CreateReactionAsync(reaction);
                await ctx.Message.RespondAsync(responseMsg);
            }

            await ctx.ConditionalWarnUserNicknameConvention();
        }
    }
}
