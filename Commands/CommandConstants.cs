﻿namespace UFOBot.Commands
{
    public static class CommandConstants
    {
        public const string SuccessReaction = ":white_check_mark:";
        public const string WarningReaction = ":warning:";
        public const string FailureReaction = ":x:";
        public const string NicknameConventionWarning = "Your nickname does not follow the server convention. Either DM me the command `!setnickname` or follow the convention of `[CorpCode*] InGameUsername | CompanyCode`.";
    }
}
