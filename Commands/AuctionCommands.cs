﻿#if DEBUG
#define USE_SECONDS_INSTEAD_OF_HOURS
#endif

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Threading.Tasks;

using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using DSharpPlus.Interactivity.Extensions;

using Microsoft.EntityFrameworkCore;

using UFOBot.Database;
using UFOBot.Database.Models;
using UFOBot.Extensions;
using UFOBot.Utils;
using UFOBot.Events;
using DSharpPlus.Interactivity;
using DSharpPlus.Interactivity.Enums;

namespace UFOBot.Commands
{
	public class AuctionCommands : BaseCommandModule
	{
		private const int AuctionSummaryLengthLimit = 100;
		private const int AuctionDescriptionLengthLimit = 1500;

		private const int AuctionCountLimit = 8;

		private const int BiddingTimeMinimumHours = 24;
		private const int BiddingTimeMaximumHours = 300;
		private static string BiddingTimeInvalidMessage = $"Bidding time must be a full hour between {BiddingTimeMinimumHours} hours and {BiddingTimeMaximumHours} hours.";

		private const int VerificationTimeMinimumHours = 24;
		private const int VerificationTimeMaximumHours = 90;
		private static string VerificatonTimeInvalidMessage = $"Verification time must be a full hour between {VerificationTimeMinimumHours} hours and {VerificationTimeMaximumHours} hours.";

		private const int SaltLengthLimit = 64;
		private const int UnhashedBidLengthLimit = 256;
		private const int RawBidLengthLimit = 350;
		private static string ExceedBidLengthLimitMessage = $"**NOTE:** The string you are hashing exceeds the bid length limit of {RawBidLengthLimit} characters.";

		private async Task<bool> EnforceDM(CommandContext ctx)
		{
			if (!ctx.IsDM())
			{
				await ctx.Message.CreateReactionAsync(DiscordEmoji.FromName(ctx.Client, CommandConstants.FailureReaction));
				var responseMessage = await ctx.Message.RespondAsync("This command is only valid within a Direct Message.");
				await Task.Delay(1500);
				await responseMessage.DeleteAsync();
				return false;
			}

			return true;
		}

		[Command("sha256hash")]
		[Description("Generate a SHA256 hash provided input text")]
		public async Task SHA256HashCommand(CommandContext ctx,
			[Description("The string to hash")]
			[RemainingText]
			string stringToHash)
		{
			if (!await EnforceDM(ctx))
			{
				return;
			}

			if (String.IsNullOrEmpty(stringToHash))
			{
				await ctx.Message.CreateReactionAsync(DiscordEmoji.FromName(ctx.Client, CommandConstants.FailureReaction));
			}

			bool bExceedsAuctionLengthLimit = stringToHash.Length > RawBidLengthLimit;
			var ConditionalExceedMessage = bExceedsAuctionLengthLimit ? $"{ExceedBidLengthLimitMessage}\n" : "";
			var hash = CalculateSHA256HashFromInput(stringToHash);

			await ctx.Message.RespondAsync($"{ConditionalExceedMessage} `{hash}`");
		}

		private const string MustBeVerifiedString = "You are unable to participate in auctions unless your discord account is verified with an in-game username. To verify, DM me `!verify`.";
		private async Task<bool> EnforceVerified(DiscordMember member)
		{
			if (!member.IsVerified())
			{
				await member.SendDM(MustBeVerifiedString);
				return false;
			}

			return true;
		}

		private static TimeSpan StartAuctionTimeoutOverride = TimeSpan.FromMinutes(10);

		[Command("startauction")]
		[Description("Allows a governor to start an auction in their planets' politics channel")]
		public async Task StartAuctionCommand(CommandContext ctx)
		{
			if (!await EnforceDM(ctx))
			{
				return;
			}

			var guild = await ctx.GetGuildAsync();
			var member = await ctx.GetMemberAsync();
			if (await EnforceVerified(member))
			{
				if (!member.IsGovernor() && !member.IsModerating())
				{
					await ctx.RespondAsync("You must be a governor or actively moderating to create an auction.");
					return;
				}

				await member.SendMessageAsync("Starting auction creation. If at any time you wish to discontinue this process, reply with `cancel`.");
				await Task.Delay(1000);

				DiscordChannel channel;
				if (member.IsModerating())
				{
					const string channelIdMessage =
						"[Moderating] What channel id do you wish to start this auction?" +
						"To retrieve the channel id, ensure `Developer Mode` is enabled under Settings->Advanced, then right-click the channel and select `Copy ID`.";
					await member.SendMessageAsync(channelIdMessage);

					var discordChannelIdResponse = await ctx.GetNextMessageAsyncAndHandleAbort(timeoutOverride: StartAuctionTimeoutOverride);
					if (discordChannelIdResponse == null) return;

					var ChannelIdStr = discordChannelIdResponse.Content;
					if (ulong.TryParse(ChannelIdStr, out ulong ChannelId))
					{
						channel = guild.GetChannel(ChannelId);
						if (channel == null)
						{
							await discordChannelIdResponse.RespondAsync("Invalid ChannelId");
							return;
						}
					}
					else
					{
						await discordChannelIdResponse.RespondAsync("Invalid ChannelId");
						return;
					}
				}
				else
				{
					channel = await member.GetGovernorPoliticsChannel();
					if (channel == null)
					{
						await ctx.RespondAsync("Invalid ChannelId");
						return;
					}
				}

				var activeAuctions = await GetActiveAuctionsAsync();
				var thisChannelActiveAuctions = activeAuctions.Where(a => a.ChannelId == channel.Id).ToList();
				if (thisChannelActiveAuctions.Count >= AuctionCountLimit)
				{
					await member.SendMessageAsync($"There are already {thisChannelActiveAuctions.Count} auctions active.  The limit is {AuctionCountLimit}. Aborting.");
					return;
				}

				await member.SendMessageAsync($"Provide a brief summary of the auction (limit of {AuctionSummaryLengthLimit} characters). It's best to simply list what it is for and what materials are requested.");
				var summaryResponse = await ctx.GetNextMessageAsyncAndHandleAbort(timeoutOverride: StartAuctionTimeoutOverride);
				if (summaryResponse == null) return;

				var summaryStr = summaryResponse.Content.Trim();
				if (summaryStr.Length > AuctionSummaryLengthLimit)
				{
					await summaryResponse.RespondAsync($"Excceded {AuctionSummaryLengthLimit} character limit. Aborting.");
					return;
				}

				string DescriptionMessageText =
					$"Provide a in-depth description of the auction (limit of {AuctionDescriptionLengthLimit} characters).\n" +
					"This description can contain have line breaks/newlines (type shift-enter to do so).\n" +
					"This should contain:\n" +
					" * What type of auction (traditional, vickrey, etc)\n" +
					" * What the auction is for\n" +
					" * The requested materials and their amounts\n" +
					" * What the payment currency/currencies is/are\n" +
					" * Maximum cost\n" +
					" * Delivery time of goods\n" +
					" * Potential contingencies when the number of bidders is under some threshold\n" +
					" * etc";
				await member.SendMessageAsync(DescriptionMessageText);

				var descriptionResponse = await ctx.GetNextMessageAsyncAndHandleAbort(timeoutOverride: StartAuctionTimeoutOverride);
				if (descriptionResponse == null) return;

				var descriptionStr = descriptionResponse.Content.Trim();
				if (descriptionStr.Length > AuctionDescriptionLengthLimit)
				{
					await descriptionResponse.RespondAsync($"Exceeded {AuctionDescriptionLengthLimit} character limit. Aborting.");
					return;
				}

				await member.SendMessageAsync($"Specify how long the bidding phase of the auction takes in hours (between {BiddingTimeMinimumHours} and {BiddingTimeMaximumHours} hours, inclusive).");
				var biddingPhaseLengthResponse = await ctx.GetNextMessageAsyncAndHandleAbort(timeoutOverride: StartAuctionTimeoutOverride);
				if (biddingPhaseLengthResponse == null) return;

				var biddingPhaseLengthStr = biddingPhaseLengthResponse.Content.Trim();
				int biddingPhaseLength;
				if (int.TryParse(biddingPhaseLengthStr, out biddingPhaseLength))
				{
					if (!member.IsModerating() && (biddingPhaseLength < BiddingTimeMinimumHours || biddingPhaseLength > BiddingTimeMaximumHours))
					{
						await biddingPhaseLengthResponse.RespondAsync(BiddingTimeInvalidMessage + " Aborting.");
						return;
					}
				}
				else
				{
					await biddingPhaseLengthResponse.RespondAsync("Failed to parse bidding time. Aborting.");
					return;
				}

				await member.SendMessageAsync($"Specify how long the verification phase of the auction takes in hours (between {VerificationTimeMinimumHours} and {VerificationTimeMaximumHours} hours, inclusive).");
				var verificationPhaseLengthResponse = await ctx.GetNextMessageAsyncAndHandleAbort(timeoutOverride: StartAuctionTimeoutOverride);
				if (verificationPhaseLengthResponse == null) return;

				var verificationPhaseLengthStr = verificationPhaseLengthResponse.Content.Trim();
				if (int.TryParse(verificationPhaseLengthStr, out int verificationPhaseLength))
				{
					if (!member.IsModerating() && (verificationPhaseLength < VerificationTimeMinimumHours || verificationPhaseLength > VerificationTimeMaximumHours))
					{
						await verificationPhaseLengthResponse.RespondAsync(VerificatonTimeInvalidMessage + " Aborting.");
						return;
					}
				}
				else
				{
					await verificationPhaseLengthResponse.RespondAsync("Failed to parse verification phase time. Aborting.");
					return;
				}

				await member.SendMessageAsync("Confirm auction creation?  **Auctions cannot be canceled**. Type 'YES' to create the auction.");
				var confirmResponse = await ctx.GetNextMessageAsyncAndHandleAbort(timeoutOverride: StartAuctionTimeoutOverride);
				if (confirmResponse == null) return;

				var confirmResponseStr = confirmResponse.Content.Trim().ToUpper();
				if (!confirmResponseStr.StartsWith("YE") && !confirmResponseStr.StartsWith("YA"))
				{
					await confirmResponse.RespondAsync("Aborted Auction Creation");
					return;
				}

				Auction auction;
				using (var ufoTransaction = new UFODbTransaction())
				{
					var lastAuction = await ufoTransaction.DB.Auctions.Where(a => a.ChannelId == channel.Id).OrderByDescending(a => a.ChannelAuctionIndex).FirstOrDefaultAsync();
					int lastAuctionIndex = (lastAuction != null) ? lastAuction.ChannelAuctionIndex : 0;

					string naturalId = null;
					channel.GetPlanetNameAndId(out naturalId, out string _);
					if (naturalId == null)
					{
						naturalId = channel.Id.ToString();
					}

					auction = new Auction();
					auction.AuctionId = $"{naturalId}-{lastAuctionIndex + 1}".ToUpper();
					auction.ChannelId = channel.Id;
					auction.PlanetNaturalId = naturalId;
					auction.ChannelAuctionIndex = lastAuctionIndex + 1;
					auction.CreationTime = DateTime.UtcNow;
#if USE_SECONDS_INSTEAD_OF_HOURS
					auction.BiddingEndTime = auction.CreationTime.AddSeconds(biddingPhaseLength);
					auction.VerificationEndTime = auction.BiddingEndTime.AddSeconds(verificationPhaseLength);
#else
					auction.BiddingEndTime = auction.CreationTime.AddHours(biddingPhaseLength);
					auction.VerificationEndTime = auction.BiddingEndTime.AddHours(verificationPhaseLength);
#endif
					auction.AuctionCancelled = false;
					auction.Summary = summaryStr;
					auction.Description = descriptionStr;

					ufoTransaction.DB.Auctions.Add(auction);
					await ufoTransaction.DB.SaveChangesAsync();
				}

				await AuctionEvents.NotifyAuctionStart(auction);
				await Task.Delay(500);
				await member.SendMessageAsync("Auction created successfully.");
			}
		}

		class BidCommandHelperArgs
		{
			public CommandContext ctx { get; set; } = null;
			public string auctionId { get; set; } = null;

			public bool EnforceDM { get; set; } = false;
			public bool EnforceVerified { get; set; } = true;
			public bool EnforceValidAuction { get; set; } = true;
			public bool EnforceInGameUsernameValid { get; set; } = true;

			public bool EnforceBiddingActive { get; set; } = false;
			public bool EnforceVerificationActive { get; set; } = false;
			public bool EnforceAuctionActive { get; set; } = false;

			public bool EnforceNotDuplicateBid { get; set; } = false;
			public bool EnforceHasABid { get; set; } = false;
		}

		class BidCommandHelperResponse
		{
			public bool HasError { get; set; } = false;

			public Auction auction { get; set; } = null;
			public DiscordMember discordMember { get; set; } = null;
			public string inGameUsername { get; set; } = null;
			public HashedBid existingHashedBid { get; set; } = null;
		}

		private async Task<BidCommandHelperResponse> AuctionCommandsCommon(BidCommandHelperArgs args)
		{
			// Assume error
			var resp = new BidCommandHelperResponse();
			resp.HasError = true;

			if (args.ctx == null)
			{
				return resp;
			}

			if (String.IsNullOrWhiteSpace(args.auctionId))
			{
				return resp;
			}

			if (args.EnforceDM && !await EnforceDM(args.ctx))
			{
				return resp;
			}

			resp.discordMember = await args.ctx.GetMemberAsync();
			if (resp.discordMember == null)
			{
				await args.ctx.RespondAsync("You are not a member of UFO discord.");
				return resp;
			}

			if (args.EnforceVerified && !await EnforceVerified(resp.discordMember))
			{
				return resp;
			}

			using (var DB = new UFODbContext())
			{
				resp.auction = await GetAuctionFromDatabaseAsync(args.auctionId, DB);
				if (resp.auction == null && args.EnforceValidAuction)
				{
					await resp.discordMember.SendDM("AuctionId specified not found.");
					return resp;
				}

				resp.inGameUsername = await GetInGameUserNameAsync(resp.discordMember, DB);
				if (resp.inGameUsername == null && args.EnforceInGameUsernameValid)
				{
					await resp.discordMember.SendDM("Unable to find your username in the verification database (this shouldn't happen). Try re-running `!verify`.");
					return resp;
				}

				if (!resp.auction.BiddingActive && args.EnforceBiddingActive)
				{
					await resp.discordMember.SendDM("Bidding is no longer active on this auction.");
					return resp;
				}

				if (!resp.auction.VerificationActive && args.EnforceVerificationActive)
				{
					await resp.discordMember.SendDM("Verification is no longer active on this auction.");
					return resp;
				}

				if (!resp.auction.AuctionActive && args.EnforceAuctionActive)
				{
					await resp.discordMember.SendDM("This auction is no longer active.");
					return resp;
				}

				resp.existingHashedBid = resp.auction.HashedBids.FirstOrDefault(hb => hb.AuctionId == resp.auction.AuctionId && hb.InGameUserName.ToUpper() == resp.inGameUsername.ToUpper());
				if (resp.existingHashedBid != null && args.EnforceNotDuplicateBid)
				{
					await resp.discordMember.SendDM("You already have an existing bid.  Please invoke `!removebid` first.");
					return resp;
				}

				if (resp.existingHashedBid == null && args.EnforceHasABid)
				{
					await resp.discordMember.SendDM("You don't have a bid on the auction.");
					return resp;
				}
			}

			resp.HasError = false;
			return resp;
		}

		private static TimeSpan BidAuctionTimeoutOverride = TimeSpan.FromMinutes(3);

		[Command("bidauction")]
		[Description("Interactive command to bid on an auction provided an AuctionId")]
		public async Task BidAuctionCommand(CommandContext ctx,
			[Description("AuctionId")]
			string AuctionId)
		{
			AuctionId = AuctionId.ToUpper();

			var args = new BidCommandHelperArgs()
			{
				ctx = ctx,
				auctionId = AuctionId,
				EnforceDM = true,
				EnforceVerified = true,
				EnforceValidAuction = true,
				EnforceInGameUsernameValid = true,
				EnforceBiddingActive = true,
				EnforceNotDuplicateBid = true
			};

			var resp = await AuctionCommandsCommon(args);
			if (resp.HasError)
			{
				return;
			}

			var auction = resp.auction;
			var discordMember = resp.discordMember;
			var inGameUsername = resp.inGameUsername;

			string EnterBidMessage =
				"__**Enter your bid**__\n" +
				$"Bid text can be a maximum of {UnhashedBidLengthLimit} characters.\n" +
				"Bids should contain at a _bare minimum_ what you wish to be paid.\n" +
				"You can use shift-enter to add newlines to your bid text.\n" +
				"Bids should also be as **clear as possible** to avoid any ambiguity.\n" +
				"Example: `Two orders of 200 MCG @ 15 CIS/unit (total of 3k CIS per 200).`\n\n" +
				"If it any time you wish to stop this process, simply type `cancel`.";
			await discordMember.SendMessageAsync(EnterBidMessage);

			var bidResponse = await ctx.GetNextMessageAsyncAndHandleAbort(timeoutOverride: BidAuctionTimeoutOverride);
			if (bidResponse == null) return;

			var bidResponseStr = bidResponse.Content.Trim();
			if (bidResponseStr.Length > UnhashedBidLengthLimit)
			{
				await discordMember.SendDM($"The bid text provided exceeded {UnhashedBidLengthLimit} characters. Aborting.");
				return;
			}

			string EnterSaltMessage =
				"__**Enter a salt**__\n" +
				"A salt is any text. This text will be appended to the end of your bid and displayed during verification.\n" +
				$"A salt can be a maximum of {SaltLengthLimit} characters.";
			await discordMember.SendMessageAsync(EnterSaltMessage);

			var saltResponse = await ctx.GetNextMessageAsyncAndHandleAbort(timeoutOverride: BidAuctionTimeoutOverride);
			if (saltResponse == null) return;

			var saltResponseStr = saltResponse.Content.Trim();
			if (saltResponseStr.Length > SaltLengthLimit)
			{
				await discordMember.SendDM($"The salt text provided exceeded {SaltLengthLimit} characters. Aborting.");
				return;
			}

			string rawInputStr = $"{bidResponseStr} Salt: {saltResponseStr}";
			string hash = CalculateSHA256HashFromInput(rawInputStr);

			HashedBid hb = new HashedBid(auction, hash, discordMember.Id, inGameUsername);
			await AddBidToDatabase(hb);

			string BidAcceptedMessage =
				"__**Bid accepted**__\n" +
				$"Bid accepted. Your hash is `{hash}`.\n" +
				"Save the following string __in its entirety__ as this is what you will need later to verify your bid.\n" +
				"I will **not** store this for you:";
			await discordMember.SendDM(BidAcceptedMessage);
			await discordMember.SendDM(rawInputStr);
			await AuctionEvents.NotifyAuctionBidAdded(auction, hb);
		}

		[Command("bidauctionraw")]
		[Description("Non-interactive command to bid on an auction. Note: The bid hash provided must reference a string which is less than 350 characters, otherwise it will not work correctly.")]
		public async Task BidRawAuctionCommand(CommandContext ctx,
			[Description("AuctionId")]
			string AuctionId,
			[Description("Bid hash")]
			string rawHash)
		{
			AuctionId = AuctionId.ToUpper();
			rawHash = rawHash.ToUpper();

			var args = new BidCommandHelperArgs()
			{
				ctx = ctx,
				auctionId = AuctionId,
				EnforceDM = true,
				EnforceVerified = true,
				EnforceValidAuction = true,
				EnforceInGameUsernameValid = true,
				EnforceBiddingActive = true,
				EnforceNotDuplicateBid = true
			};

			var resp = await AuctionCommandsCommon(args);
			if (resp.HasError)
			{
				return;
			}

			var auction = resp.auction;
			var discordMember = resp.discordMember;
			var inGameUsername = resp.inGameUsername;

			if (!rawHash.IsSHA256Hash())
			{
				await discordMember.SendDM($"`{rawHash}` is not a valid SHA256 hash.");
				return;
			}

			HashedBid hb = new HashedBid(auction, rawHash, discordMember.Id, inGameUsername);
			await AddBidToDatabase(hb);

			await discordMember.SendDM("Bid accepted.");
			await AuctionEvents.NotifyAuctionBidAdded(auction, hb);
		}

		[Command("removebid")]
		[Description("Remove your bid from the provided AuctionId")]
		public async Task RemoveBidCommand(CommandContext ctx,
			[Description("AuctionId")]
			string AuctionId)
		{
			AuctionId = AuctionId.ToUpper();

			var args = new BidCommandHelperArgs()
			{
				ctx = ctx,
				auctionId = AuctionId,
				EnforceVerified = true,
				EnforceValidAuction = true,
				EnforceInGameUsernameValid = true,
				EnforceHasABid = true
			};

			var resp = await AuctionCommandsCommon(args);
			if (resp.HasError)
			{
				return;
			}

			var auction = resp.auction;
			var discordMember = resp.discordMember;

			bool bAuctionCancelled = false;
			using (var ufoTransaction = new UFODbTransaction())
			{
				var hb = await ufoTransaction.DB.HashedBids.FirstOrDefaultAsync(hb => hb.AuctionId == auction.AuctionId && hb.DiscordUserId == discordMember.Id);
				var ub = await ufoTransaction.DB.UnhashedBids.FirstOrDefaultAsync(hb => hb.AuctionId == auction.AuctionId && hb.DiscordUserId == discordMember.Id);

				if (hb != null)
				{
					ufoTransaction.DB.HashedBids.Remove(hb);
				}
				if (ub != null)
				{
					ufoTransaction.DB.UnhashedBids.Remove(ub);
				}

				var auc = await ufoTransaction.DB.Auctions.Include(a => a.HashedBids).FirstOrDefaultAsync(a => a.AuctionId == auction.AuctionId);
				if (auc.VerificationActive && auc.HashedBids.Count == 0)
				{
					bAuctionCancelled = true;
				}

				ufoTransaction.DB.SaveChanges();
			}

			await discordMember.SendDM("Your bid was removed.");

			await AuctionEvents.NotifyAuctionBidRemoved(auction, resp.existingHashedBid);

			if (bAuctionCancelled)
			{
				await AuctionEvents.ConditionalCancelAuction(auction.AuctionId, AuctionEvents.AuctionEventType.BiddingEnded);
				await AuctionEvents.NotifyChannelOfAuctionEvent(auction.AuctionId, AuctionEvents.AuctionEventType.BiddingEnded);
			}
		}

		[Command("verifybid")]
		[Description("Command to verify a bid on an auction")]
		public async Task VerifyBidCommand(CommandContext ctx,
			[Description("AuctionId")]
			string AuctionId,
			[RemainingText]
			[Description("Original Text")]
			string originalText)
		{
			AuctionId = AuctionId.ToUpper();

			var args = new BidCommandHelperArgs()
			{
				ctx = ctx,
				auctionId = AuctionId,
				EnforceVerified = true,
				EnforceValidAuction = true,
				EnforceInGameUsernameValid = true,
				EnforceVerificationActive = true,
				EnforceHasABid = true
			};

			var resp = await AuctionCommandsCommon(args);
			if (resp.HasError)
			{
				return;
			}

			var auction = resp.auction;
			var discordMember = resp.discordMember;
			var inGameUsername = resp.inGameUsername;

			if (originalText.Length > 350)
			{
				await discordMember.SendDM("Provided original text is too large.");
				return;
			}

			var originalTextHash = CalculateSHA256HashFromInput(originalText);
			if (resp.existingHashedBid.SHA256Hash != originalTextHash)
			{
				await discordMember.SendDM($"The original text provide with SHA256Hash `{originalTextHash}` does not match the database hash of `{resp.existingHashedBid.SHA256Hash}`.");
				return;
			}

			UnhashedBid ub = new UnhashedBid(auction, originalTextHash, originalText, discordMember.Id, inGameUsername);
			await AddVerificationToDatabase(ub);

			await discordMember.SendDM("Your bid has been verified.");
			await AuctionEvents.NotifyAuctionVerificationAdded(auction, ub);

			// Re-retrieve the auction object to see if we're done with this auction
			auction = await GetAuctionFromDatabaseAsync(AuctionId);
			if (auction.UnhashedBids.Count == auction.HashedBids.Count)
			{
				await AuctionEvents.ConditionalCancelAuction(AuctionId, AuctionEvents.AuctionEventType.VerificationEnded);
				await AuctionEvents.NotifyUsersOfAuctionEvent(AuctionId, AuctionEvents.AuctionEventType.VerificationEnded);
				await AuctionEvents.NotifyChannelOfAuctionEvent(AuctionId, AuctionEvents.AuctionEventType.VerificationEnded);
			}
		}

		private DiscordEmbedBuilder GetAuctionOverviewEmbed(Auction auction)
		{
			string status;
			if (auction.AuctionActive)
			{
				if (auction.BiddingActive)
				{
					status = "Active (Bidding Phase)";
				}
				else
				{
					status = "Active (Verification Phase)";
				}
			}
			else
			{
				if (auction.AuctionHadNoBids)
				{
					status = "Cancelled. No bids.";
				}
				else if (auction.AuctionHadNoVerifications)
				{
					status = "Cancelled. No verifications.";
				}
				else
				{
					status = "Complete.";
				}
			}

			var OverviewEmbed = new DiscordEmbedBuilder()
					.AddField("AuctionId", auction.AuctionId)
					.AddField("Status", status)
					.AddField("Creation Time", $"<t:{auction.CreationTime.ToEpochSeconds()}>")
					.AddField("Bidding End Time", $"<t:{auction.BiddingEndTime.ToEpochSeconds()}>")
					.AddField("Verification End Time", $"<t:{auction.VerificationEndTime.ToEpochSeconds()}>")
					.AddField("Bid Count", $"{auction.HashedBids.Count}")
					.AddField("Summary", auction.Summary)
					.WithDescription(auction.Description);

			return OverviewEmbed;
		}

		[Command("viewauction")]
		[Description("View the progress/results of an auction")]
		public async Task ViewAuctionCommand(CommandContext ctx,
			[Description("AuctionId")]
			string AuctionId)
		{
			AuctionId = AuctionId.ToUpper();

			var args = new BidCommandHelperArgs()
			{
				ctx = ctx,
				auctionId = AuctionId,
				EnforceValidAuction = true,
			};

			var resp = await AuctionCommandsCommon(args);
			if (resp.HasError)
			{
				return;
			}

			var discordMember = resp.discordMember;
			var auction = resp.auction;

			var OverviewEmbed = GetAuctionOverviewEmbed(auction);
			if (ctx.IsDM())
			{
				var dmChannel = await discordMember.CreateDmChannelAsync();
				if (dmChannel != null)
				{
					var pages = new List<Page>();
					pages.Add(new Page(embed: OverviewEmbed));

					for (int hashedBidIdx = 0; hashedBidIdx < auction.HashedBids.Count; ++hashedBidIdx)
					{
						var hashedBid = auction.HashedBids[hashedBidIdx];
						var unhashedBid = auction.UnhashedBids.FirstOrDefault(ub => ub.InGameUserName == hashedBid.InGameUserName);

						var PageEmbed = new DiscordEmbedBuilder()
							.AddField("UserName", hashedBid.InGameUserName, inline: true)
							.AddField("Bid Verified", (unhashedBid != null) ? "Yes" : "No", inline: true)
							.AddField("SHA256Hash", hashedBid.SHA256Hash)
							.WithFooter($"Bid {hashedBidIdx + 1}/{auction.HashedBids.Count}");
						if (unhashedBid != null)
						{
							PageEmbed.WithDescription(unhashedBid.UnhashedString);
						}
						pages.Add(new Page(embed: PageEmbed));
					}

					await dmChannel.SendPaginatedMessageAsync(ctx.User, pages, new PaginationEmojis(), behaviour: PaginationBehaviour.Ignore, deletion: PaginationDeletion.DeleteMessage, timeoutoverride: TimeSpan.FromMinutes(3.0));
				}
			}
			else
			{
				await ctx.RespondAsync(OverviewEmbed);
			}
		}

		[Command("viewallauctions")]
		[Description("View all available active auctions")]
		public async Task ViewAllAuctionsCommand(CommandContext ctx)
		{
			if (!await EnforceDM(ctx))
			{
				return;
			}

			var discordMember = await ctx.GetMemberAsync();
			if (discordMember == null)
			{
				await ctx.RespondAsync("You are not a member of UFO discord.");
			}

			var dmChannel = await discordMember.CreateDmChannelAsync();
			if (dmChannel != null)
			{
				var pages = new List<Page>();
				var activeAuctions = await GetActiveAuctionsAsync();
				foreach (var auction in activeAuctions)
				{
					var PageEmbed = new DiscordEmbedBuilder()
						.AddField("AuctionId", auction.AuctionId)
						.AddField("Creation Time", $"<t:{auction.CreationTime.ToEpochSeconds()}>")
						.AddField("Bidding End Time", $"<t:{auction.BiddingEndTime.ToEpochSeconds()}>")
						.AddField("Verification End Time", $"<t:{auction.VerificationEndTime.ToEpochSeconds()}>")
						.AddField("Summary", auction.Summary)
						.WithDescription(auction.Description);
					pages.Add(new Page(embed: PageEmbed));
				}

				if (pages.Count == 0)
				{
					await dmChannel.SendMessageAsync("There are currently no active auctions.");
				}
				else
				{
					await dmChannel.SendPaginatedMessageAsync(ctx.User, pages, new PaginationEmojis(), behaviour: PaginationBehaviour.Ignore, deletion: PaginationDeletion.DeleteMessage, timeoutoverride: TimeSpan.FromMinutes(3.0));
				}
			}
		}

		private async Task<bool> AddBidToDatabase(HashedBid hashedBid)
		{
			using (var ufoTransaction = new UFODbTransaction())
			{
				var auction = await ufoTransaction.DB.Auctions.FirstOrDefaultAsync(a => a.AuctionId.ToUpper() == hashedBid.AuctionId.ToUpper());
				if (auction != null)
				{
					ufoTransaction.DB.HashedBids.Add(hashedBid);
					await ufoTransaction.DB.SaveChangesAsync();
					return true;
				}

				return false;
			}
		}

		private async Task<bool> AddVerificationToDatabase(UnhashedBid unhashedBid)
		{
			using (var ufoTransaction = new UFODbTransaction())
			{
				var auction = await ufoTransaction.DB.Auctions.FirstOrDefaultAsync(a => a.AuctionId == unhashedBid.AuctionId);
				if (auction != null)
				{
					ufoTransaction.DB.UnhashedBids.Add(unhashedBid);

					await ufoTransaction.DB.SaveChangesAsync();
					return true;
				}

				return false;
			}
		}

		private async Task<Auction> GetAuctionFromDatabaseAsync(string auctionId, UFODbContext DB = null)
		{
			bool bCreatedDb = (DB == null);
			if (bCreatedDb)
			{
				DB = new UFODbContext();
			}

			Auction auction = await DB.Auctions.Include(a => a.HashedBids).Include(a => a.UnhashedBids).FirstOrDefaultAsync(a => a.AuctionId.ToUpper() == auctionId);

			if (bCreatedDb)
			{
				await DB.DisposeAsync();
				DB = null;
			}

			return auction;
		}

		public static async Task<string> GetInGameUserNameAsync(DiscordMember member, UFODbContext DB = null)
		{
			bool bCreatedDb = (DB == null);
			if (bCreatedDb)
			{
				DB = new UFODbContext();
			}

			var model = await DB.InGameUserToDiscordUserModels.FirstOrDefaultAsync(m => m.DiscordId == member.Id);

			if (bCreatedDb)
			{
				await DB.DisposeAsync();
				DB = null;
			}

			return model?.InGameUsername;
		}

		public static async Task<List<Auction>> GetActiveAuctionsAsync(UFODbContext DB = null)
		{
			bool bCreatedDb = (DB == null);
			if (bCreatedDb)
			{
				DB = new UFODbContext();
			}

			var thresholdPrior = DateTime.UtcNow.AddMonths(-1);
			List<Auction> potentiallyActiveAuctions = await DB.Auctions.Where(a => a.CreationTime > thresholdPrior).ToListAsync();

			if (bCreatedDb)
			{
				await DB.DisposeAsync();
				DB = null;
			}

			// Pare this down to for realz active auctions
			return potentiallyActiveAuctions.Where(a => a.AuctionActive).ToList();
		}

		public static string CalculateSHA256HashFromInput(string input)
		{
			using (SHA256 sha256Hash = SHA256.Create())
			{
				byte[] hashBytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
				var sb = new StringBuilder();
				foreach (var hashByte in hashBytes)
				{
					sb.Append(hashByte.ToString("x2"));
				}
				return sb.ToString().ToUpper();
			}
		}
	}
}
