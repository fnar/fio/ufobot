﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using System.Threading.Tasks;

using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.EventArgs;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using DSharpPlus.Interactivity;
using DSharpPlus.Interactivity.Enums;
using DSharpPlus.Interactivity.Extensions;

using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Serilog;

using UFOBot.Extensions;
using UFOBot.Database;
using UFOBot.Database.Models;
using UFOBot.Utils;

namespace UFOBot.Commands
{
    class AdminCommands : BaseCommandModule
    {
        [Command("escalate")]
        [Description("Enable moderator mode on an administrator")]
        public async Task PromoteCommand(CommandContext ctx,
            [RemainingText]
            string reason)
        {
            if (reason == null || reason.Length <= 3)
            {
                return;
            }

            DiscordMember member = await ctx.GetMemberAsync();
            if (member == null)
            {
                await ctx.RespondAsync("Unable to identify you as a member in the guild.");
                return;
            }

            bool bPromoted = member.IsModerator() && !member.IsModerating() && await member.GrantModerating();
            if (bPromoted)
            {
                var nameToDispaly = member.DisplayName;

                var auditChannel = await ctx.GetAuditLogChannelAsync();
                string auditMessage = $"**{nameToDispaly} escalated.**\nReason: \n > " + reason;
                await auditChannel.SendMessageAsync(auditMessage);
            }

            DiscordEmoji reaction = (bPromoted ? DiscordEmoji.FromName(ctx.Client, CommandConstants.SuccessReaction) : DiscordEmoji.FromName(ctx.Client, CommandConstants.FailureReaction));

            if (!ctx.IsDM() && !ctx.Channel.IsManagedChannel())
            {
                // Send new DM response
                var dm = await ctx.SendDMResponseWithOriginalCommand();
                if (dm != null)
                {
                    await dm.CreateReactionAsync(reaction);
                }

                // Delete the original message
                await ctx.Message.DeleteAsync();
            }
            else
            {
                await ctx.Message.CreateReactionAsync(reaction);
            }
        }

        [Command("deescalate")]
        [Description("Remove moderator mode from administrator")]
        public async Task DemoteCommand(CommandContext ctx)
        {
            await DemoteCommand(ctx, null);
        }

        [Command("deescalate")]
        public async Task DemoteCommand(CommandContext ctx,
            [RemainingText]
            string comment)
        {
            DiscordMember member = await ctx.GetMemberAsync();
            if (member == null)
            {
                await ctx.RespondAsync("Unable to identify you as a member in the guild.");
                return;
            }

            bool bUnpromoted = member.IsModerating() && await member.UngrantModerating();
            if (bUnpromoted)
            {
                var nameToDispaly = (member.Nickname != null ? member.Nickname : member.DisplayName);

                var auditChannel = await ctx.GetAuditLogChannelAsync();

                string auditChannelMessage = $"**{nameToDispaly} de-escalated.**";
                if (!string.IsNullOrWhiteSpace(comment))
                {
                    auditChannelMessage += "\nComment: \n > " + comment;
                }
                await auditChannel.SendMessageAsync(auditChannelMessage);
            }

            DiscordEmoji reaction = (bUnpromoted ? DiscordEmoji.FromName(ctx.Client, CommandConstants.SuccessReaction) : DiscordEmoji.FromName(ctx.Client, CommandConstants.FailureReaction));

            if (!ctx.IsDM() && !ctx.Channel.IsManagedChannel())
            {
                // Send new DM response
                var dm = await ctx.SendDMResponseWithOriginalCommand();
                if (dm != null)
                {
                    await dm.CreateReactionAsync(reaction);
                }

                // Delete the original message
                await ctx.Message.DeleteAsync();
            }
            else
            {
                await ctx.Message.CreateReactionAsync(reaction);
            }
        }

        [Command("clearcompany")]
        [Description("Clears a provided company code from the database.")]
        public async Task ClearCompany(CommandContext ctx,
            [Description("CompanyCode")]
            string CompanyCode)
        {
            if (CompanyCode.Length >= 1 && CompanyCode.Length <= 4)
            {
                DiscordMember member = await ctx.GetMemberAsync();
                if (member != null && member.IsModerating())
                {
                    using (var ufoTransaction = new UFODbTransaction())
                    {
                        var discUserModel = await ufoTransaction.DB.DiscordUsers.FirstOrDefaultAsync(du => du.CompanyCode.ToUpper() == CompanyCode.ToUpper());
                        if (discUserModel != null)
                        {
                            var guild = await ctx.GetGuildAsync();
                            if (guild != null)
                            {
                                DiscordMember discUserModelMember = null;
                                try
                                {
                                    discUserModelMember = await guild.GetMemberAsync(discUserModel.DiscordId);
                                }
                                catch
                                {

                                }
                                
                                if (discUserModelMember != null)
                                {
                                    await discUserModelMember.ModifyAsync(x =>
                                    {
                                        x.Nickname = null;
                                        x.AuditLogReason = "Removed nickname by moderator action.";
                                    });
                                }

                                ufoTransaction.DB.DiscordUsers.Remove(discUserModel);
                                await ufoTransaction.DB.SaveChangesAsync();
                                await ctx.Message.CreateReactionAsync(DiscordEmoji.FromName(ctx.Client, CommandConstants.SuccessReaction));
                                return;
                            }
                        }
                    }
                }
            }

            await ctx.Message.CreateReactionAsync(DiscordEmoji.FromName(ctx.Client, CommandConstants.FailureReaction));
        }

        [Command("viewmessage")]
        [Description("Views message history. This command requires moderating.\n" +
            "Ex: `viewmessage <MessageLink>`")]
        public async Task ViewMessageCommand(CommandContext ctx,
            [Description("MessageLink")]
            string MessageLink)
        {
            DiscordMember member = await ctx.GetMemberAsync();
            if (member != null && member.IsModerating())
            {
                var dmChannel = await member.CreateDmChannelAsync();
                if (dmChannel != null)
                {
                    // Do interactive bits
                    if (MessageLink.GetMessageLinkParts(out ulong CategoryId, out ulong ChannelId, out ulong MessageId))
                    {
                        var pages = new List<Page>();

                        using (var db = new UFODbContext())
                        {
                            var msg = db.DiscordMessageModels.FirstOrDefault(dmm => dmm.MessageId == MessageId);
                            if (msg != null)
                            {
                                var OverviewEmbed = new DiscordEmbedBuilder()
                                    .AddField("Author", $"{msg.AuthorUsername}")
                                    .AddField("Send Date", msg.TimestampUtc.ToString("O"))
                                    .AddField("Deleted", msg.Deleted ? $"{msg.DeletedTimestampUtc:O}" : "No")
                                    .WithDescription(msg.Content)
                                    .WithFooter("Original Message");
                                pages.Add(new Page(embed: OverviewEmbed));

                                for (int editIdx = 0; editIdx < msg.Edits.Count; ++editIdx)
                                {
                                    var edit = msg.Edits[editIdx];

                                    var PageEmbed = new DiscordEmbedBuilder()
                                        .AddField("Author", $"{msg.AuthorUsername}")
                                        .AddField("Send Date", $"{edit.TimestampUtc:O}")
                                        .AddField("Deleted", msg.Deleted ? $"{msg.DeletedTimestampUtc:O}" : "No")
                                        .WithDescription(edit.Content)
                                        .WithFooter($"Edit #{editIdx + 1}");
                                    pages.Add(new Page(embed: PageEmbed));
                                }
                            }
                        }

                        if (pages.Count > 0)
                        {
                            if (!ctx.IsDM() && !ctx.Channel.IsManagedChannel())
                            {
                                await ctx.Message.DeleteAsync();
                            }

                            await dmChannel.SendPaginatedMessageAsync(ctx.User, pages, new PaginationEmojis(), behaviour: PaginationBehaviour.Ignore, deletion: PaginationDeletion.DeleteMessage, timeoutoverride: TimeSpan.FromMinutes(3.0));
                            return;
                        }

                    }
                }
            }

            if (!ctx.IsDM() && !ctx.Channel.IsManagedChannel())
            {
                await ctx.Message.DeleteAsync();
            }
        }

        [Command("channelintegritycheck")]
        [Description("[Moderator Only] Verifies all channel role permissions")]
        public async Task ChannelIntegrityCheckCommand(CommandContext ctx)
        {
            DiscordMember member = await ctx.GetMemberAsync();
            if (member != null && member.IsModerating())
            {
                var dmChannel = await member.CreateDmChannelAsync();
                if (dmChannel != null)
                {
#if WITH_COMMUNITY_MODE
                    await dmChannel.SendMessageAsync("Integrity check not valid with community mode");
#else
                    await dmChannel.SendMessageAsync("Executing");
                    await ValidateChannelPermissions(await ctx.GetGuildAsync());
                    await dmChannel.SendMessageAsync("Channel Integrity Check Complete.");
#endif
                }
            }
            else
            {
                await ctx.Message.CreateReactionAsync(DiscordEmoji.FromName(ctx.Client, CommandConstants.FailureReaction));
            }
        }

        [Command("fixupchannels")]
        [Description("[Moderator Only] Verifies and fixes up all planet channels")]
        public async Task FixupChannelsCommand(CommandContext ctx)
        {
            DiscordMember member = await ctx.GetMemberAsync();
            if (member != null && member.IsModerating())
            {
                var dmChannel = await member.CreateDmChannelAsync();
                if (dmChannel != null)
                {
#if WITH_COMMUNITY_MODE
                    await dmChannel.SendMessageAsync("Fixup channels not enabled with community mode");
#else
                    await dmChannel.SendMessageAsync("Executing");
                    await PopulateAndFixupChannels(await ctx.GetGuildAsync());
                    await dmChannel.SendMessageAsync("Fixup Channels Complete.");
#endif // WITH_COMMUNITY_MODE
                }
            }
            else
            {
                await ctx.Message.CreateReactionAsync(DiscordEmoji.FromName(ctx.Client, CommandConstants.FailureReaction));
            }
        }

        [Command("fixupgovernorroles")]
        [Description("[Moderator Only] Verifies and fixes up all governor roles")]
        public async Task FixupGovernorRolesCommand(CommandContext ctx)
        {
            DiscordMember member = await ctx.GetMemberAsync();
            if (member != null && member.IsModerating())
            {
                var dmChannel = await member.CreateDmChannelAsync();
                if (dmChannel != null)
                {
                    await dmChannel.SendMessageAsync("Executing");
                    await FixUpGovernorRoles(await ctx.GetGuildAsync());
                    await dmChannel.SendMessageAsync("Fixup Governor Roles Complete.");
                }
            }
            else
            {
                await ctx.Message.CreateReactionAsync(DiscordEmoji.FromName(ctx.Client, CommandConstants.FailureReaction));
            }
        }

        private static async Task<bool> SetChannelRolePermissions(DiscordChannel channel, DiscordGuild guild)
        {
            ConfigurationSection discordConfig = (ConfigurationSection)Globals.configuration.GetSection("Discord");
            // Don't modify the protected channel(s):
            string[] ignoredCategories = discordConfig.GetSection("IgnoredCategories").Get<string[]>();
            if (channel.Parent != null && ignoredCategories.Contains(channel.Parent.Name.ToUpper()))
            {
                Log.Debug($"Not setting permissions on: {channel.Parent.Name} > {channel.Name}");
                return false;
            }
            else if (channel.Parent == null && ignoredCategories.Contains(channel.Name.ToUpper()))
            {
                Log.Debug($"Not setting permissions on: {channel.Name}");
                return false;
            }
            List<DiscordRole> roles = guild.Roles
                .Where(x => x.Value.Name.ToUpper() == discordConfig.GetValue<string>("ModeratorPromotedRole").ToUpper())
                .Select(x => x.Value)
                .ToList();
            if (roles.Count == 0)
            {
                Log.Error($"Failed to find role for {discordConfig.GetValue<string>("ModeratorPromotedRole")}");
                return false;
            }

            var AllowPerms =
                Permissions.AccessChannels |
                Permissions.ManageMessages |
                Permissions.ManageChannels |
                Permissions.SendMessages;
            var DenyPerms = Permissions.None;
            Log.Debug($"Provide permission: {channel.Name}");
            await channel.AddOverwriteAsync(roles[0], AllowPerms, DenyPerms);
            return true;
        }

        private static async Task AutoAddUsersToChannel(DiscordClient client, DiscordGuild guild, DiscordChannel channel)
        {
            ConfigurationSection discordConfig = (ConfigurationSection)Globals.configuration.GetSection("Discord");
            string[] ignoredCategories = discordConfig.GetSection("IgnoredCategories").Get<string[]>();
            if (channel.Parent != null && !ignoredCategories.Contains(channel.Parent.Name.ToUpper()))
            {
                List<DiscordMember> usersToAutoAdd = new List<DiscordMember>();

                // Grab all channels underneath the newly created channel, except _this_ channel.
                // We're going to grab the intersection of all this category's channels
                var candidateChannels = channel.Parent.Children.Where(c => c.Name.ToUpper() != channel.Name.ToUpper()).ToList();
                if (candidateChannels.Count > 0)
                {
                    // Member overwrite & AccessChannels check
                    var candidateChannelOverwrites = candidateChannels[0].PermissionOverwrites.Where(po => po.Type == OverwriteType.Member && po.CheckPermission(Permissions.AccessChannels) == PermissionLevel.Allowed);
                    foreach (var overwrite in candidateChannelOverwrites)
                    {
                        usersToAutoAdd.Add(await overwrite.GetMemberAsync());
                        await Task.Delay(250);
                    }

                    for (int candidateChannelIdx = 1; candidateChannelIdx < candidateChannels.Count; ++candidateChannelIdx)
                    {
                        List<DiscordMember> usersWithRead = new List<DiscordMember>();
                        candidateChannelOverwrites = candidateChannels[0].PermissionOverwrites.Where(po => po.Type == OverwriteType.Member && po.CheckPermission(Permissions.AccessChannels) == PermissionLevel.Allowed);
                        foreach (var overwrite in candidateChannelOverwrites)
                        {
                            usersWithRead.Add(await overwrite.GetMemberAsync());
                            await Task.Delay(250);
                        }

                        usersToAutoAdd = usersToAutoAdd.Intersect(usersWithRead).ToList();
                    }
                }

                foreach (var userToAutoAdd in usersToAutoAdd)
                {
                    var allowPerms = Permissions.AccessChannels;
                    var denyPerms = Permissions.None;
                    await channel.AddOverwriteAsync(userToAutoAdd, allowPerms, denyPerms);
                    await Task.Delay(500);
                }

                if (usersToAutoAdd.Count > 0)
                {
                    string memberS = usersToAutoAdd.Count > 1 ? "s" : "";
                    await channel.SendMessageAsync($"Auto-added {usersToAutoAdd.Count} member{memberS} to this channel.  To leave, type `!leave`");
                }
            }
        }

        public static async Task ChannelCreatedEvent(DiscordClient client, ChannelCreateEventArgs evt)
        {
            Log.Debug("AdminCommands: Channel Created");
#if !WITH_COMMUNITY_MODE
            _ = SetChannelRolePermissions(evt.Channel, evt.Guild);
            _ = AutoAddUsersToChannel(client, evt.Guild, evt.Channel);
#endif // !WITH_COMMUNITY_MODE
            await Task.FromResult(0);
        }

        private static DiscordGuild guild = null;
        public static async Task GuildDownloadCompletedEvent(DiscordClient client, GuildDownloadCompletedEventArgs evt)
        {
            Log.Debug("AdminCommands: Guild Download Completed");

            guild = evt.Guilds.Values.First();

            _ = Task.Run(async () =>
            {
                await Globals.DataRetrieval.GetSettledPlanetDefs(ForceRetrieve: true);

                await UpgradeInGameUserToDiscordUserTable(guild);

#if !WITH_COMMUNITY_MODE
                await PopulateAndFixupChannels(guild);
                await ValidateChannelPermissions(guild);
#endif // !WITH_COMMUNITY_MODE
                await FixUpGovernorRoles(guild);
            });

            StartGuildUpdateTimer();

            await Task.FromResult(0);
        }

        public static async Task ValidateChannelPermissions(DiscordGuild guild)
        {
            foreach (KeyValuePair<ulong, DiscordChannel> channelinfo in guild.Channels)
            {
                DiscordChannel channel = channelinfo.Value;
                _ = await SetChannelRolePermissions(channel, guild);
            }
        }

        public static async Task UpgradeInGameUserToDiscordUserTable(DiscordGuild guild)
        {
            using (var ufoTransaction = new UFODbTransaction())
            {
                var allMembers = await guild.GetAllMembersAsync();
                var mappingsToUpgrade = ufoTransaction.DB.InGameUserToDiscordUserModels.Where(map => map.DiscordId == 0);
                foreach (var mappingToUpgrade in mappingsToUpgrade)
                {
                    var member = allMembers.FirstOrDefault(member => member.Id == mappingToUpgrade.DiscordId);
                    if (member != null)
                    {
                        mappingToUpgrade.DiscordId = member.Id;
                    }
                }

                await ufoTransaction.DB.SaveChangesAsync();
            }
        }

        public class PlanetNameAndGovernor
        {
            public string PlanetName { get; set; }
            public string Username { get; set; }
        }

        private const int BaseCountThreshold = 25;

        //[Command("populateplanetchannels")]
        //[Description("Populate planet channels and fix them up. `!channelintegritycheck` is necessary after running this")]]
        public static async Task PopulateAndFixupChannels(DiscordGuild guild)
        {
            const string generalChannelStr = "general";
            const string tradeChannelStr = "shipping-trade";
            const string politicsChannelStr = "politics";

            var FixupsDone = new List<string>();
            var settledPlanetDefs = await Globals.DataRetrieval.GetSettledPlanetDefs();
            var workingSettledPlanetDefs = JsonConvert.DeserializeObject<List<DataAPI.PlanetDef>>(JsonConvert.SerializeObject(settledPlanetDefs)); // Hack: Deep copy settledPlanetDefs
            if (workingSettledPlanetDefs == null)
            {
                // Soft-fail if we get nothing back
                return;
            }
            var deletePlanetDefs = settledPlanetDefs.Where(pd => pd.BaseCount < BaseCountThreshold).ToList();

            var createPlanetsDefs = settledPlanetDefs.Where(pd => pd.BaseCount >= BaseCountThreshold).ToList();

            var planetCategories = guild.GetPlanetCategories();

            foreach (var planetCategory in planetCategories)
            {
                planetCategory.GetPlanetNameAndId(out string planetNaturalId, out string planetName);

                // First, check to see if the category needs renaming
                bool bPlanetNamed = false;
                var planetDef = workingSettledPlanetDefs.FirstOrDefault(spd => spd.NaturalId.ToUpper() == planetNaturalId.ToUpper());
                if (planetDef == null)
                {
                    continue;
                }

                if (planetDef.Name.ToUpper() != planetName.ToUpper())
                {
                    // Planet was named, category name needs update
                    var newPlanetCategoryName = $"{planetDef.Name.ToUpper()} | {planetDef.NaturalId.ToUpper()}";
                    await planetCategory.ModifyAsync(c =>
                    {
                        c.Name = newPlanetCategoryName;
                        c.AuditLogReason = $"{planetDef.NaturalId} received name {planetDef.Name}";

                        FixupsDone.Add($"'{planetDef.NaturalId}' was named '{planetDef.Name}'");
                    });
                    await Task.Delay(1000);
                    bPlanetNamed = true;
                }

                var textChannelPlanetName = planetDef.Name.Replace(" ", "-").ToLower();

                DiscordChannel generalChannel = planetCategory.Children.FirstOrDefault(pc => pc.Name.ToLower().EndsWith($"-{generalChannelStr}") || pc.Name.ToLower() == generalChannelStr);
                DiscordChannel tradeChannel = planetCategory.Children.FirstOrDefault(pc => pc.Name.ToLower().EndsWith($"-{tradeChannelStr}") || pc.Name.ToLower() == tradeChannelStr);
                DiscordChannel politicsChannel = planetCategory.Children.FirstOrDefault(pc => pc.Name.ToLower().EndsWith($"-{politicsChannelStr}") || pc.Name.ToLower() == politicsChannelStr);

                bool bNeedsGeneral = (generalChannel == null);
                bool bNeedsShippingTrade = (tradeChannel == null && (planetDef.HasLM || planetDef.HasWarehouse));
                bool bNeedsPolitics = politicsChannel == null && planetDef.HasADM;
                foreach (var planetChannel in planetCategory.Children)
                {
                    bool bNeedsFixup = false;
                    var planetChannelName = planetChannel.Name.ToLower();

                    // First, see if the channel follows server "prefix" naming conventions
                    if (!planetChannelName.StartsWith(textChannelPlanetName + "-"))
                    {
                        bNeedsFixup = true;
                    }

                    if (bNeedsFixup)
                    {
                        await planetChannel.ModifyAsync(c =>
                        {
                            c.Name = $"{textChannelPlanetName}-{planetChannelName}";
                            c.AuditLogReason = $"{planetChannelName} required fixup";
                        });
                        await Task.Delay(1000);
                    }

                    if (bPlanetNamed && planetChannelName.Contains(planetNaturalId.ToLower()))
                    {
                        await planetChannel.ModifyAsync(c =>
                        {
                            c.Name = planetChannelName.Replace(planetNaturalId.ToLower(), textChannelPlanetName);
                            c.AuditLogReason = $"{planetNaturalId.ToLower()} received name {textChannelPlanetName}";
                        });
                        await Task.Delay(1000);
                    }
                }

                // Only create channels if the 5 base threshold has been hit
                bool bHasHitBaseCountThreshold = planetDef.BaseCount >= BaseCountThreshold;
                if (bHasHitBaseCountThreshold)
                {
                    Tuple<bool, DiscordChannel> result;
                    if (bNeedsGeneral)
                    {
                        result = await guild.GuardedCreateChannelAsync($"{planetDef.Name.ToLower()}-{generalChannelStr}", ChannelType.Text, parent: planetCategory, reason: $"{generalChannelStr} not detected on {planetDef.Name}");
                        if (result.Item1)
                        {
                            generalChannel = result.Item2;
                            FixupsDone.Add($"Created missing general chat for {planetDef.Name}");
                            await Task.Delay(1000);
                        }
                    }

                    if (bNeedsShippingTrade)
                    {
                        result = await guild.GuardedCreateChannelAsync($"{planetDef.Name.ToLower()}-{tradeChannelStr}", ChannelType.Text, parent: planetCategory, reason: $"WAR or LM detected on {planetDef.Name}");
                        if (result.Item1)
                        {
                            FixupsDone.Add($"WAR or LM detected for {planetDef.Name}, created shipping-trade channel");
                            await Task.Delay(1000);

                            var generalChannelAnnouncement = $"**New Channel Added:**\nType `!join {planetDef.Name.ToLower()} {tradeChannelStr}` to join.";
                            await generalChannel.SendMessageAsync(generalChannelAnnouncement);
                            await Task.Delay(1000);
                        }
                    }

                    if (bNeedsPolitics)
                    {
                        result = await guild.GuardedCreateChannelAsync($"{planetDef.Name.ToLower()}-{politicsChannelStr}", ChannelType.Text, parent: planetCategory, reason: $"ADM detected on {planetDef.Name}");
                        if (result.Item1)
                        {
                            await Task.Delay(1000);
                            FixupsDone.Add($"ADM detected for {planetDef.Name}, created politics channel");

                            var generalChannelAnnouncement = $"**New Channel Added:**\nType `!join {planetDef.Name.ToLower()} {politicsChannelStr}` to join.";
                            await generalChannel.SendMessageAsync(generalChannelAnnouncement);
                            await Task.Delay(1000);
                        }
                    }
                }

                // Remove this def since it's been processed (already existed in some fashion)
                workingSettledPlanetDefs.Remove(planetDef);
            }

            // Go through the remaining defs and create those categories
            foreach (var settledPlanetDef in workingSettledPlanetDefs.Where(pd => pd.BaseCount >= BaseCountThreshold))
            {
                var recruitmentCategoryPosition = guild.Channels.Values.Where(ch => ch.Parent == null && ch.Name.ToUpper().Contains("RECRUITMENT")).Select(c => c.Position).First();

                var categoryName = (settledPlanetDef.Name != settledPlanetDef.NaturalId) ? $"{settledPlanetDef.Name.ToLower()} | {settledPlanetDef.NaturalId.ToLower()}" : settledPlanetDef.NaturalId.ToLower();
                var newPlanetCategoryResult = await guild.GuardedCreateChannelAsync(categoryName, ChannelType.Category, reason: $"{settledPlanetDef.Name.ToLower()} settled.");
                if (newPlanetCategoryResult.Item1)
                {
                    var newPlanetCategory = newPlanetCategoryResult.Item2;
                    await newPlanetCategory.ModifyPositionAsync(recruitmentCategoryPosition + 1); // Move it just above recruitment
                    await Task.Delay(1000);

                    await guild.GuardedCreateChannelAsync($"{settledPlanetDef.Name.ToLower()}-{generalChannelStr}", ChannelType.Text, parent: newPlanetCategory, reason: $"{generalChannelStr} chat for new planet {settledPlanetDef.Name}");
                    await Task.Delay(1000);

                    if (settledPlanetDef.HasLM || settledPlanetDef.HasWarehouse)
                    {
                        await guild.GuardedCreateChannelAsync($"{settledPlanetDef.Name.ToLower()}-{tradeChannelStr}", ChannelType.Text, parent: newPlanetCategory, reason: $"{tradeChannelStr} chat for new planet {settledPlanetDef.Name}");
                        await Task.Delay(1000);
                    }

                    if (settledPlanetDef.HasADM)
                    {
                        await guild.GuardedCreateChannelAsync($"{settledPlanetDef.Name.ToLower()}-{politicsChannelStr}", ChannelType.Text, parent: newPlanetCategory, reason: $"{politicsChannelStr} chat for new planet {settledPlanetDef.Name}");
                        await Task.Delay(1000);
                    }

                    FixupsDone.Add($"Newly settled planet category & groups created for {settledPlanetDef.Name}");
                }
            }

            if (FixupsDone.Count > 0)
            {
                FixupsDone.Add($"Current Channels & Categories Count: {guild.Channels.Count}");
            }

            // If we're at the channel limit, don't emit anything
            if (guild.Channels.Count < 500)
            {
                // Just don't emit anything (for now)
                await guild.EmitMessagesToAuditLog("Populate & Fixup Operations Done:", FixupsDone);
            }
        }

        //[MethodImpl(MethodImplOptions.NoOptimization)]
        public static async Task FixUpGovernorRoles(DiscordGuild guild)
        {
            var settledPlanetDefs = await Globals.DataRetrieval.GetSettledPlanetDefs();
            if (settledPlanetDefs == null)
            {
                // Bail, no data
                return;
            }
            var governorChanges = new List<string>();

            var allMembers = await guild.GetAllMembersAsync();
            var currentDiscordMembersWithGovernorRole = allMembers.Where(m => m.IsGovernor());

            var newPlanetsAndGovernors = settledPlanetDefs.Select(spd => new PlanetNameAndGovernor { PlanetName = spd.Name, Username = spd.GovernorUsername });
            List<InGameUserToDiscordUserModel> verifiedModels;
            using (var DB = new UFODbContext())
            {
                verifiedModels = DB.InGameUserToDiscordUserModels.ToList();
            }

            var shouldBeGovernorModels =
                from newGovernorToPlanetName in newPlanetsAndGovernors
                join verifiedModel in verifiedModels
                    on newGovernorToPlanetName.Username equals verifiedModel.InGameUsername
                select verifiedModel;

            var discordMembersThatShouldHaveGovernorRole =
                from member in allMembers
                join shouldBeGovernor in shouldBeGovernorModels
                    on member.Id equals shouldBeGovernor.DiscordId
                select member;

            var discordMembersThatNeedToHaveGovernorRoleRemoved = currentDiscordMembersWithGovernorRole.Except(discordMembersThatShouldHaveGovernorRole);
            foreach (var discordMemberThatNeedsGovernorRoleRemoved in discordMembersThatNeedToHaveGovernorRoleRemoved)
            {
                await discordMemberThatNeedsGovernorRoleRemoved.UngrantGovernor();
                await Task.Delay(1000);

                governorChanges.Add($"Revoked Governor role from '{discordMemberThatNeedsGovernorRoleRemoved.DisplayName}'");
            }

            var discordMembersThatNeedToHaveGovernorRoleAdded = discordMembersThatShouldHaveGovernorRole.Except(currentDiscordMembersWithGovernorRole);
            foreach (var discordMemberThatNeedsGovernorRoleAdded in discordMembersThatNeedToHaveGovernorRoleAdded)
            {
                await discordMemberThatNeedsGovernorRoleAdded.GrantGovernor();
                await Task.Delay(1000);

                var username = shouldBeGovernorModels.First(sbgm => sbgm.DiscordId == discordMemberThatNeedsGovernorRoleAdded.Id).InGameUsername;
                var planet = newPlanetsAndGovernors.First(npag => npag.Username != null && npag.Username.ToUpper() == username.ToUpper()).PlanetName;
                governorChanges.Add($"Assigned Governor role to {discordMemberThatNeedsGovernorRoleAdded.DisplayName} (Governor of {planet})");
            }

            await guild.EmitMessagesToAuditLog("Governor Changes:", governorChanges);
        }

        private static int guildUpdateTimerFrequency = (int)new TimeSpan(hours: 0, minutes: 10, seconds: 0).TotalMilliseconds;
        private static Timer guildUpdateTimer = new Timer(guildUpdateTimerFrequency);
        public static void StartGuildUpdateTimer()
        {
            guildUpdateTimer.Elapsed += OnGuildUpdateTimer;
            guildUpdateTimer.AutoReset = true;
            guildUpdateTimer.Enabled = true;
        }

        private static void OnGuildUpdateTimer(object source, ElapsedEventArgs e)
        {
            _ = Task.Run(async () =>
            {
                await Globals.DataRetrieval.GetSettledPlanetDefs(ForceRetrieve: true);

#if !WITH_COMMUNITY_MODE
                await PopulateAndFixupChannels(guild);
#endif // !WITH_COMMUNITY_MODE
                await FixUpGovernorRoles(guild);
            });
        }

        public static async Task Ready(DiscordClient client, ReadyEventArgs args)
        {
            List<DiscordGuild> guilds = client.Guilds.Select(x => x.Value).ToList();
            if (guilds.Count > 1)
            {
                Log.Warning($"I'm in more than 1 guild! {guilds.Count}");
            }
            ConfigurationSection discordConfig = (ConfigurationSection)Globals.configuration.GetSection("Discord");
            ulong managedGuildId = discordConfig.GetValue<ulong>("ManagedGuildID");
            if (!guilds.Select(x => x.Id).Contains(managedGuildId))
            {
                Log.Warning($"ManagedGuildID {managedGuildId} not found in Guild list.");
            }

            await Task.FromResult(0); // Suppress "This async method lacks 'await' operators" warning.
        }
    }
}
