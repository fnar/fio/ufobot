﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using Serilog;

using UFOBot.Extensions;
using UFOBot.Utils;

namespace UFOBot.Commands
{
	public class GovernorCommands : BaseCommandModule
	{
		[Command("pin")]
		[Description("Pins a linked comment url that is in this channel")]
		public async Task PinCommand(CommandContext ctx,
			[Description("MessageLink")]
			string MessageLink)
		{
			bool SuccessfullyPinned = false;

			try
			{
				if (MessageLink.GetMessageLinkParts(out ulong CategoryId, out ulong ChannelId, out ulong MessageId))
				{
					DiscordGuild guild = await ctx.GetGuildAsync();
					DiscordMember member = await ctx.GetMemberAsync();
					var channelAccess = await member.GetGovernorChannelAccess();

					var messageChannel = guild.GetChannel(ChannelId);
					if (messageChannel != null && channelAccess.Contains(messageChannel))
					{
						var message = await messageChannel.GetMessageAsync(MessageId);
						if (message != null)
						{
							await ctx.Message.CreateReactionAsync(DiscordEmoji.FromName(ctx.Client, CommandConstants.SuccessReaction));
							await message.PinAsync();
							if (!ctx.Message.IsDM())
							{
								await Task.Delay(500);
								await ctx.Message.DeleteAsync();
							}

							SuccessfullyPinned = true;
						}
					}
				}
			}
			catch
			{
				// Just fall through
			}


			if (!SuccessfullyPinned)
			{
				await ctx.Message.CreateReactionAsync(DiscordEmoji.FromName(ctx.Client, CommandConstants.FailureReaction));
				if (!ctx.Message.IsDM())
				{
					await Task.Delay(250);
					await ctx.Message.DeleteAsync();
				}
			}
		}

		[Command("unpin")]
		[Description("Unpins a linked comment url that is in this channel")]
		public async Task UnpinCommand(CommandContext ctx,
			[Description("MessageLink")]
			string MessageLink)
		{
			bool SuccessfullyUnpinned = false;

			try
			{
				if (MessageLink.GetMessageLinkParts(out ulong CategoryId, out ulong ChannelId, out ulong MessageId))
				{
					DiscordGuild guild = await ctx.GetGuildAsync();
					DiscordMember member = await ctx.GetMemberAsync();
					var channelAccess = await member.GetGovernorChannelAccess();

					var messageChannel = guild.GetChannel(ChannelId);
					if (channelAccess.Contains(messageChannel))
					{
						var pinnedMessages = await messageChannel.GetPinnedMessagesAsync();
						var messageToUnpin = pinnedMessages.FirstOrDefault(pm => pm.Id == MessageId);
						if (messageToUnpin != null)
						{
							await ctx.Message.CreateReactionAsync(DiscordEmoji.FromName(ctx.Client, CommandConstants.SuccessReaction));
							await messageToUnpin.UnpinAsync();
							if (!ctx.Message.IsDM())
							{
								await Task.Delay(500);
								await ctx.Message.DeleteAsync();
							}
							SuccessfullyUnpinned = true;
						}
					}
				}
			}
			catch
			{
				// Just fall through
			}


			if (!SuccessfullyUnpinned)
			{
				await ctx.Message.CreateReactionAsync(DiscordEmoji.FromName(ctx.Client, CommandConstants.FailureReaction));
				if (!ctx.Message.IsDM())
				{
					await Task.Delay(250);
					await ctx.Message.DeleteAsync();
				}
			}
		}

		//[Command("createchannel")]
		//[Description("Creates a channel provided a name that fits")]
		//public async Task CreateChannelCommand(CommandContext ctx)
		//{
		//	await Task.FromResult(0);
		//}
		//
		//[Command("createreadonlychannel")]
		//[Description("Creates a read-only channel provided a name that fits")]
		//public async Task CreateReadOnlyChannel(CommandContext ctx)
		//{
		//	await Task.FromResult(0);
		//}
		//
		//[Command("deletechannel")]
		//[Description("Deletes a governor created channel")]
		//public async Task DeleteChannel(CommandContext ctx)
		//{
		//	await Task.FromResult(0);
		//}
	}
}
