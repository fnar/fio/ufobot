using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using DSharpPlus.Interactivity.EventHandling;
using Microsoft.Extensions.Configuration;
using Serilog;

using UFOBot.Extensions;

namespace UFOBot.Commands
{
    /// <summary>
    /// This is the command used to join channels/channel groups
    /// </summary>
    public class ChannelCommands : BaseCommandModule
    {
        private void SetChannelOverwritesInBackground(DiscordMember member, DiscordChannel channel, DSharpPlus.Permissions allowPermissions, DSharpPlus.Permissions denyPermissions)
        {
            SetChannelOverwritesInBackground(member, new List<DiscordChannel> { channel }, allowPermissions, denyPermissions);
        }

        private void SetChannelOverwritesInBackground(DiscordMember member, List<DiscordChannel> channels, DSharpPlus.Permissions allowPermissions, DSharpPlus.Permissions denyPermissions)
        {
            _ = Task.Run(async () =>
            {
                foreach (DiscordChannel channel in channels)
                {
                    Log.Debug($"Provide permission: {(channel.Parent == null ? "" : channel.Parent.Name)} > {channel.Name}");
                    await channel.AddOverwriteAsync(member, allowPermissions, denyPermissions);
                }
            });
        }

        private async Task RespondJoinLeave(CommandContext ctx, List<DiscordChannel> channels, List<string> errorSet)
        {
            DiscordEmoji reaction = null;
            StringBuilder messageResponseSB = new StringBuilder();

            bool bSuccess = channels.Count > 0 && errorSet.Count == 0;
            if (bSuccess)
            {
                reaction = DiscordEmoji.FromName(ctx.Client, CommandConstants.SuccessReaction);
            }
            else if (channels.Count > 0 && errorSet.Count > 0)
            {
                reaction = DiscordEmoji.FromName(ctx.Client, CommandConstants.WarningReaction);
                messageResponseSB.AppendLine("Partial success.  Errors:");
                messageResponseSB.Append(String.Join("\n", errorSet.Select(x => $"> {x}")));
            }
            else
            {
                reaction = DiscordEmoji.FromName(ctx.Client, CommandConstants.FailureReaction);
                messageResponseSB.AppendLine("Failure on all inputs:");
                messageResponseSB.Append(String.Join("\n", errorSet.Select(x => $"> {x}")));
            }

            if (!ctx.IsDM() && !ctx.Channel.IsManagedChannel())
            {
                // Send a DM back
                var dm = await ctx.SendDMResponseWithOriginalCommand(messageResponseSB.ToString());
                if (dm != null)
                {
                    await dm.CreateReactionAsync(reaction);
                }

                // Delete the original message
                await ctx.Message.DeleteAsync();
            }
            else
            {
                // Otherwise, just react (writing a message on failure)
                await ctx.Message.CreateReactionAsync(reaction);
                if (!bSuccess)
                {
                    await ctx.Channel.SendMessageAsync(messageResponseSB.ToString());
                }
            }

            await ctx.ConditionalWarnUserNicknameConvention();
        }

        [Command("join")]
        [Description("Joins you to a specified set of channels.\n"+
            "Ex: `join Etherwind` joins you to all channels in the Etherwind category.\n" +
            "Ex: `join Katoa general` joins you to just the general channel in the Katoa category.\n"+
            "To join multiple category/channels at once, separate them with a comma.\n"+
            "Ex: `join Katoa general, Etherwind general shipping-trade, Hortus`")]
        public async Task JoinCommand(CommandContext ctx, 
            [Description("Set of category/channels")]
            [RemainingText]
            string args)
        {
#if WITH_COMMUNITY_MODE_NO_JOIN
            var reaction = DiscordEmoji.FromName(ctx.Client, CommandConstants.FailureReaction);
            await ctx.Message.CreateReactionAsync(reaction);
#else
            DiscordGuild guild = await ctx.GetGuildAsync();
            DiscordMember member = await ctx.GetMemberAsync();
            if (member == null )
            {
                await ctx.RespondAsync("Unable to identify you as a member in the guild.");
                return;
            }

            List<string> errorSet = new List<string>();

            // Get channels to update
            var AllowPerms = DSharpPlus.Permissions.AccessChannels;
            var DenyPerms = DSharpPlus.Permissions.None;
            List<DiscordChannel> channels = ChannelsFromCatChanString(args, guild, errorSet);
            SetChannelOverwritesInBackground(member, channels, AllowPerms, DenyPerms);

            await RespondJoinLeave(ctx, channels, errorSet);
#endif
        }

        [Command("joinallplanets")]
        [Description("Joins you to all planet chats. You monster.")]
        public async Task JoinAllPlanetsCommand(CommandContext ctx)
        {
#if WITH_COMMUNITY_MODE
            var reaction = DiscordEmoji.FromName(ctx.Client, CommandConstants.FailureReaction);
            await ctx.Message.CreateReactionAsync(reaction);
#else
            DiscordGuild guild = await ctx.GetGuildAsync();
            DiscordMember member = await ctx.GetMemberAsync();
            if (member == null)
            {
                await ctx.RespondAsync("Unable to identify you as a member in the guild.");
                return;
            }

            var channels = guild.GetPlanetCategories().SelectMany(c => c.Children).ToList();
            var AllowPerms = DSharpPlus.Permissions.AccessChannels;
            var DenyPerms = DSharpPlus.Permissions.None;
            SetChannelOverwritesInBackground(member, channels, AllowPerms, DenyPerms);

            var dummyErrorSet = new List<string>();
            await RespondJoinLeave(ctx, channels, dummyErrorSet);
#endif
        }

        [Command("joinallfactions")]
        [Description("Joins you to all faction chats.")]
        public async Task JoinAllFactionsCommand(CommandContext ctx)
        {
#if WITH_COMMUNITY_MODE_NO_JOIN
            var reaction = DiscordEmoji.FromName(ctx.Client, CommandConstants.FailureReaction);
            await ctx.Message.CreateReactionAsync(reaction);
#else
            DiscordGuild guild = await ctx.GetGuildAsync();
            DiscordMember member = await ctx.GetMemberAsync();
            if (member == null)
            {
                await ctx.RespondAsync("Unable to identify you as a member in the guild.");
                return;
            }

            var channels = guild.GetFactionCategories().SelectMany(c => c.Children).ToList();
            var AllowPerms = DSharpPlus.Permissions.AccessChannels;
            var DenyPerms = DSharpPlus.Permissions.None;
            SetChannelOverwritesInBackground(member, channels, AllowPerms, DenyPerms);

            var dummyErrorSet = new List<string>();
            await RespondJoinLeave(ctx, channels, dummyErrorSet);
#endif
        }

        [Command("joinallgeneral")]
        [Description("Joins you to all general chats.")]
        public async Task JoinAllGeneralCommand(CommandContext ctx)
        {
#if WITH_COMMUNITY_MODE_NO_JOIN
            var reaction = DiscordEmoji.FromName(ctx.Client, CommandConstants.FailureReaction);
            await ctx.Message.CreateReactionAsync(reaction);
#else
            DiscordGuild guild = await ctx.GetGuildAsync();
            DiscordMember member = await ctx.GetMemberAsync();
            if (member == null)
            {
                await ctx.RespondAsync("Unable to identify you as a member in the guild.");
                return;
            }

            var channels = guild.GetGeneralCategories().SelectMany(c => c.Children).ToList();
            var AllowPerms = DSharpPlus.Permissions.AccessChannels;
            var DenyPerms = DSharpPlus.Permissions.None;
            SetChannelOverwritesInBackground(member, channels, AllowPerms, DenyPerms);

            var dummyErrorSet = new List<string>();
            await RespondJoinLeave(ctx, channels, dummyErrorSet);
#endif
        }

        [Command("leaveallplanets")]
        [Description("Leave all planet chats.")]
        public async Task LeaveAllPlanetsCommand(CommandContext ctx)
        {
#if WITH_COMMUNITY_MODE_NO_JOIN
            var reaction = DiscordEmoji.FromName(ctx.Client, CommandConstants.FailureReaction);
            await ctx.Message.CreateReactionAsync(reaction);
#else
            DiscordGuild guild = await ctx.GetGuildAsync();
            DiscordMember member = await ctx.GetMemberAsync();
            if (member == null)
            {
                await ctx.RespondAsync("Unable to identify you as a member in the guild.");
                return;
            }

            var channels = guild.GetPlanetCategories().SelectMany(c => c.Children).ToList();
            var AllowPerms = DSharpPlus.Permissions.None;
            var DenyPerms = DSharpPlus.Permissions.AccessChannels;
            SetChannelOverwritesInBackground(member, channels, AllowPerms, DenyPerms);

            var dummyErrorSet = new List<string>();
            await RespondJoinLeave(ctx, channels, dummyErrorSet);
#endif
        }

        [Command("leaveallfactions")]
        [Description("Leave all faction chats.")]
        public async Task LeaveAllFactionsCommand(CommandContext ctx)
        {
#if WITH_COMMUNITY_MODE_NO_JOIN
            var reaction = DiscordEmoji.FromName(ctx.Client, CommandConstants.FailureReaction);
            await ctx.Message.CreateReactionAsync(reaction);
#else
            DiscordGuild guild = await ctx.GetGuildAsync();
            DiscordMember member = await ctx.GetMemberAsync();
            if (member == null)
            {
                await ctx.RespondAsync("Unable to identify you as a member in the guild.");
                return;
            }

            var channels = guild.GetFactionCategories().SelectMany(c => c.Children).ToList();
            var AllowPerms = DSharpPlus.Permissions.None;
            var DenyPerms = DSharpPlus.Permissions.AccessChannels;
            SetChannelOverwritesInBackground(member, channels, AllowPerms, DenyPerms);

            var dummyErrorSet = new List<string>();
            await RespondJoinLeave(ctx, channels, dummyErrorSet);
#endif
        }

        [Command("leaveallgeneral")]
        [Description("Leave all general chats.")]
        public async Task LeaveAllGeneralCommand(CommandContext ctx)
        {
#if WITH_COMMUNITY_MODE_NO_JOIN
            var reaction = DiscordEmoji.FromName(ctx.Client, CommandConstants.FailureReaction);
            await ctx.Message.CreateReactionAsync(reaction);
#else
            DiscordGuild guild = await ctx.GetGuildAsync();
            DiscordMember member = await ctx.GetMemberAsync();
            if (member == null)
            {
                await ctx.RespondAsync("Unable to identify you as a member in the guild.");
                return;
            }

            var channels = guild.GetGeneralCategories().SelectMany(c => c.Children).ToList();
            var AllowPerms = DSharpPlus.Permissions.None;
            var DenyPerms = DSharpPlus.Permissions.AccessChannels;
            SetChannelOverwritesInBackground(member, channels, AllowPerms, DenyPerms);

            var dummyErrorSet = new List<string>();
            await RespondJoinLeave(ctx, channels, dummyErrorSet);
#endif
        }

        [Command("leave")]
        [Description("Removes you to a specified set of channels (or current channel if blank).\n" +
            "Ex: `leave Etherwind` removes you from all channels in the Etherwind category.\n" +
            "Ex: `leave Katoa general` removes you from just the general channel in the Katoa category.\n" +
            "To leave multiple category/channels at once, separate them with a comma.\n" +
            "Ex: `leave Katoa general, Etherwind general shipping-trade, Hortus`")]
        public async Task LeaveCommand(CommandContext ctx)
        {
#if WITH_COMMUNITY_MODE_NO_JOIN
            var reaction = DiscordEmoji.FromName(ctx.Client, CommandConstants.FailureReaction);
            await ctx.Message.CreateReactionAsync(reaction);
#else
            if (ctx.IsDM())
            {
                // If DM, ignore.  Can't leave "nothing."
                return;
            }

            DiscordChannel channel = ctx.Channel;
            if (channel.IsInIgnoredCategory())
            {
                // If the command was done in an ignored category, do nothing.
                return;
            }

            var member = await ctx.GetMemberAsync();

            var AllowPerms = DSharpPlus.Permissions.None;
            var DenyPerms = DSharpPlus.Permissions.AccessChannels;
            Log.Debug($"Provide permission: {channel.Name}");
            SetChannelOverwritesInBackground(member, channel, AllowPerms, DenyPerms);
            await ctx.Message.DeleteAsync();
#endif
        }

        [Command("leave")]
        public async Task LeaveCommand(CommandContext ctx,
            [Description("Set of category/channels")]
            [RemainingText]
            string args)
        {
#if WITH_COMMUNITY_MODE_NO_JOIN
            var reaction = DiscordEmoji.FromName(ctx.Client, CommandConstants.FailureReaction);
            await ctx.Message.CreateReactionAsync(reaction);
#else
            DiscordGuild guild = await ctx.GetGuildAsync();
            DiscordMember member = await ctx.GetMemberAsync();
            if (member == null)
            {
                await ctx.RespondAsync("Unable to identify you as a member in the guild.");
                return;
            }

            List<string> errorSet = new List<string>();

            // Get channels to update
            var AllowPerms = DSharpPlus.Permissions.None;
            var DenyPerms = DSharpPlus.Permissions.AccessChannels;
            List<DiscordChannel> channels = ChannelsFromCatChanString(args, guild, errorSet);
            SetChannelOverwritesInBackground(member, channels, AllowPerms, DenyPerms);

            await RespondJoinLeave(ctx, channels, errorSet);
#endif
        }

        [Command("list")]
        [Description("List the Category (Planet) sections you can join, or the individual channels within a Category.")]
        public async Task ListCategoryCommand(CommandContext ctx)
        {
#if WITH_COMMUNITY_MODE_NO_JOIN
            var reaction = DiscordEmoji.FromName(ctx.Client, CommandConstants.FailureReaction);
            await ctx.Message.CreateReactionAsync(reaction);
#else
            DiscordGuild guild = await ctx.GetGuildAsync();
            if (guild == null)
            {
                await ctx.RespondAsync("[BOT ERROR] Guild not properly set up.");
                return;
            }

            DiscordMember member = await ctx.GetMemberAsync();
            if (member == null)
            {
                await ctx.RespondAsync("Unable to identify you as a member in the guild.");
                return;
            }

            List<DiscordChannel> planetCategories = guild.GetPlanetCategories();
            List<DiscordChannel> factionCategories = guild.GetFactionCategories();
            List<DiscordChannel> generalCategories = guild.GetGeneralCategories();

            DiscordEmbedBuilder response = new DiscordEmbedBuilder();
            response.WithColor(new DiscordColor(79, 138, 16));
            response.Title = "List of channel categories (groups)";
            if(generalCategories.Count > 0)
            {
                response.AddField("General:", String.Join("\n", generalCategories.Select(x => x.Name)), true);
            }
            if(factionCategories.Count > 0)
            {
                response.AddField("Faction Space:", String.Join("\n", factionCategories.Select(x => x.Name)), true);
            }
            if(planetCategories.Count > 0)
            {
                int planetGroupSize = 10;
                int firstGroupSize = Math.Max(generalCategories.Count, factionCategories.Count);
                if (planetCategories.Count > 5 && planetCategories.Count < 30 + firstGroupSize)
                {
                    // Split the rest into 3
                    planetGroupSize = (planetCategories.Count - firstGroupSize+3) / 3;
                }
                if (planetCategories.Count > 200)
                {
                    // Max 25 fields in embed, but we'll make the first only 10 anyway (pretty)
                    planetGroupSize = (planetCategories.Count - firstGroupSize) / 21;
                }
                // build first group
                {
                    List<string> names = new List<string>();
                    for (int jdx = 0; jdx < firstGroupSize; jdx++)
                    {
                        if (jdx >= planetCategories.Count)
                        {
                            break;
                        }
                        names.Add(planetCategories[jdx].Name);
                    }
                    response.AddField("Planets:", String.Join("\n", names), true);
                }
                for (int idx = 0; idx < (planetCategories.Count - firstGroupSize) / planetGroupSize + 1; idx++)
                {
                    int rangeSize = Math.Min(planetGroupSize, planetCategories.Count - (idx * planetGroupSize + firstGroupSize));
                    Log.Debug($"Getting planet names: {idx * planetGroupSize + firstGroupSize} => {idx * planetGroupSize + firstGroupSize + rangeSize}");
                    List<string> names = planetCategories
                        .GetRange(idx * planetGroupSize + firstGroupSize, rangeSize)
                        .Select(x => x.Name)
                        .ToList();
                    response.AddField("Planets (Cont):", String.Join("\n", names), true);
                }
            }

            if (!ctx.IsDM() && !ctx.Channel.IsManagedChannel())
            {
                // Send a DM back
                await ctx.SendDMResponseWithOriginalCommand(response);

                // Delete the original message
                await ctx.Message.DeleteAsync();
            }
            else
            {
                await ctx.RespondAsync(response);
            }
#endif
        }
        [Command("list")]
        public async Task ListCategoryCommand(CommandContext ctx, 
            [Description("Category/Planet for which to list channels")]
            string category)
        {
#if WITH_COMMUNITY_MODE_NO_JOIN
            var reaction = DiscordEmoji.FromName(ctx.Client, CommandConstants.FailureReaction);
            await ctx.Message.CreateReactionAsync(reaction);
#else
            DiscordGuild guild = await ctx.GetGuildAsync();
            ConfigurationSection discordConfig = (ConfigurationSection)Globals.configuration.GetSection("Discord");

            string[] ignoredCategories = discordConfig.GetSection("IgnoredCategories").Get<string[]>();
            List<DiscordChannel> categories = guild.Channels.Values
                .Where(channel => channel.Parent == null && ignoredCategories.Contains(channel.Name.ToUpper()) == false)
                .ToList();
            List<DiscordChannel> results = categories.Where(channel => channel.Name.ToUpper().Contains(category.ToUpper())).ToList();
            Log.Debug($"Command(list, category): Result.Count = {results.Count}");

            DiscordEmbedBuilder response = new DiscordEmbedBuilder();
            if(results.Count == 0)
            {
                response.WithColor(new DiscordColor(255, 0, 0));
                response.Title = "Failure";
                response.AddField("No matching category/planet:", $"{category}");
            }
            else if (results.Count > 1)
            {
                response.WithColor(new DiscordColor(216, 0, 12));
                response.Title = $"Error: Found multiple matches for \"{category}\"";
                response.AddField("Specify from matches:", "```\n" + String.Join(", ", results.Select(x => x.Name)) + "\n```");
            }
            else
            {
                response.WithColor(new DiscordColor(79, 138, 16));
                response.Title = $"Channels within \"{results[0].Name}\"";
                response.AddField("Channels", "```\n" + String.Join("\n", results[0].Children.Select(x => x.Name)) + "\n```");
            }

            if (!ctx.IsDM() && !ctx.Channel.IsManagedChannel())
            {
                // Send a DM back
                await ctx.SendDMResponseWithOriginalCommand(response);

                // Delete the original message
                await ctx.Message.DeleteAsync();
            }
            else
            {
                await ctx.RespondAsync(response);
            }
#endif
        }


        /*
         * Get a list of channels based on the `category channel, cat2 chan2 chan3` string
         */
        private List<DiscordChannel> ChannelsFromCatChanString(string input, DiscordGuild guild, List<string> errorSet)
        {
            List<DiscordChannel> channels = new List<DiscordChannel>();
            ConfigurationSection discordConfig = (ConfigurationSection)Globals.configuration.GetSection("Discord");

            string[] in_sections = input.Split(",").Select(x => x.Trim()).ToArray();
            List<DiscordChannel> categories = guild.Channels.Values.Where(channel => channel.Parent == null).ToList();
            Regex chanrgx = new Regex(@"[^\s""']+|""([^""]*)""|'([^']*)'");
            Log.Debug($"Input sections: \n\t{String.Join("\n\t", in_sections)}");
            foreach (string in_section in in_sections)
            {
                MatchCollection matches = chanrgx.Matches(in_section);
                Log.Debug($"input category match count: {matches.Count}");
                if (matches.Count == 0)
                {
                    // @TODO: nothing found.  Let user know that garbage in = garbage out
                    errorSet.Add("Garbage in; Bypass to trash.");
                    break;
                }
                if (matches.Count == 1)
                {
                    // Just the category found.  Treat it as "all".
                    List<DiscordChannel> catResults = categories.Where(channel => channel.Name.ToUpper().Contains(matches[0].Value.ToUpper())).ToList();
                    if (catResults.Count == 1)
                    {
                        string[] ignored = discordConfig.GetSection("IgnoredCategories").Get<string[]>();
                        if (ignored.Contains(catResults[0].Name.ToUpper()))
                        {
                            errorSet.Add($"Unknown category: {matches[0]}");
                            break;
                        }

                        foreach (var subchannel in catResults[0].Children)
                        {
                            channels.Add(subchannel);
                        }
                    }
                    else
                    {
                        errorSet.Add($"Unable to find category matching: {matches[0]}");
                    }
                }
                if (matches.Count > 1)
                {
                    // Category + channels found.
                    List<DiscordChannel> catResults = categories.Where(channel => channel.Name.ToUpper().Contains(matches[0].Value.ToUpper())).ToList();
                    if (catResults.Count == 1)
                    {
                        string[] ignored = discordConfig.GetSection("IgnoredCategories").Get<string[]>();
                        if (ignored.Contains(catResults[0].Name.ToUpper()))
                        {
                            errorSet.Add($"Unusable category: {catResults[0].Name}");
                            break;
                        }
                        Log.Debug($"Found category: {catResults[0].Name}");

                        for (int idx = 1; idx < matches.Count; idx++)
                        {
                            List<DiscordChannel> chanResults = catResults[0].Children.Where(channel => channel.Name.ToUpper().Contains(matches[idx].Value.ToUpper())).ToList();
                            Log.Debug($"Finding {matches[idx].Value} in {catResults[0].Name} => {chanResults.Count} entries");
                            if (chanResults.Count == 1)
                            {
                                channels.Add(chanResults[0]);
                            }
                            else
                            {
                                errorSet.Add($"Unable to match \"{matches[idx]}\" to a single channel in {catResults[0].Name}");
                            }
                        }
                    }
                }
            }
            return channels;
        }
    }
}
