﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using DSharpPlus.Interactivity.Extensions;
using Microsoft.EntityFrameworkCore;
using UFOBot.Database;
using UFOBot.Database.Models;
using UFOBot.Extensions;

namespace UFOBot.Commands
{
    public class ServerCommands : BaseCommandModule
    {
        [Command("setnickname")]
        [Description("Set your nickname following server conventions\n**NOTE:** This must be run as a Direct Message to the bot!")]
        public async Task SetNicknameCommand(CommandContext ctx)
        {
            if (!ctx.IsDM())
            {
                var msg = await ctx.RespondAsync("This command is only valid when direct messaging the bot.");
                await Task.Delay(3000);
                await msg.DeleteAsync();
            }
            else
            {
                var member = await ctx.GetMemberAsync();
                var corpMsg = await ctx.RespondAsync("Are you in a corporation?  Respond with `Yes` or `No`.");

                string corpCode = null;
                string username = null;
                string companyCode = null;

                var yesNoMsg = await ctx.Message.GetNextMessageAsync();
                if (yesNoMsg.TimedOut) return;

                var yesNoContent = yesNoMsg.Result.Content.ToUpper().Trim();
                if (yesNoContent == "Y" || yesNoContent.StartsWith("YE") || yesNoContent.StartsWith("YA"))
                {
                    await yesNoMsg.Result.RespondAsync("Please type your corporation code");

                    bool bInvalidCorpCode = true;
                    while (bInvalidCorpCode)
                    {
                        var corpNextMsg = await ctx.Message.GetNextMessageAsync();
                        if (corpNextMsg.TimedOut) return;

                        corpCode = corpNextMsg.Result.Content.ToUpper().Trim();
                        if (Regex.IsMatch(corpCode, @"^[A-Z0-9]{1,4}$"))
                        {
                            bInvalidCorpCode = false;
                        }
                        else
                        {
                            await ctx.RespondAsync("Invalid coproration code specified.  Try again.");
                        }
                    }
                }

                await member.SendMessageAsync("Please type your APEX username. ");
                bool bInvalidUserName = true;
                while (bInvalidUserName)
                {
                    var usernameNextMsg = await ctx.Message.GetNextMessageAsync();
                    if (usernameNextMsg.TimedOut) return;

                    username = usernameNextMsg.Result.Content.Trim();
                    if (Regex.IsMatch(username, @"^[^\[\]\s]+$"))
                    {
                        bInvalidUserName = false;
                    }
                    else
                    {
                        await usernameNextMsg.Result.RespondAsync("Invalid username specified.  Try again.");
                    }
                }

                await member.SendMessageAsync("Please enter your company **code**. (This is 1 to 4 characters).\nTo find your company code, in APEX, click your name in the top-right. The first row in the new buffer that shows is has your company code.");
                bool bInvalidCompanyCode = true;
                while (bInvalidCompanyCode)
                {
                    var companyCodeNextMsg = await ctx.Message.GetNextMessageAsync();
                    if (companyCodeNextMsg.TimedOut) return;

                    companyCode = companyCodeNextMsg.Result.Content.ToUpper().Trim();
                    if (Regex.IsMatch(companyCode, @"^[A-Z0-9]{1,4}$"))
                    {
                        bInvalidCompanyCode = false;
                    }
                    else
                    {
                        await companyCodeNextMsg.Result.RespondAsync("Invalid company code specified.  Try again.");
                    }
                }

                string NewNickname = "";
                if (corpCode != null)
                {
                    NewNickname = $"[{corpCode}] {username} | {companyCode}";
                }
                else
                {
                    NewNickname = $"{username} | {companyCode}";
                }

                if (NewNickname.Length > 32)
                {
                    int numLastChars = NewNickname.Length - 32;
                    username = username.Substring(0, username.Length - numLastChars);
                    if (corpCode != null)
                    {
                        NewNickname = $"[{corpCode}] {username} | {companyCode}";
                    }
                    else
                    {
                        NewNickname = $"{username} | {companyCode}";
                    }
                }

                var discordMember = await ctx.GetMemberAsync();
                await discordMember.ModifyAsync(x => x.Nickname = NewNickname);
                await member.SendMessageAsync($"Nickname set to `{NewNickname}`");
            }
        }

        [Command("verify")]
        [Description("Verify and link your in-game username to your discord username")]
        public async Task Verify(CommandContext ctx)
        {
            if (!ctx.IsDM())
            {
                var msg = await ctx.RespondAsync("This command is only valid when direct messaging the bot.");
                await Task.Delay(3000);
                await msg.DeleteAsync();
            }
            else
            {
                var discordMember = await ctx.GetMemberAsync();
                if (!discordMember.NicknameMatchesConvention())
                {
                    await ctx.RespondAsync("Your nickname does not match server convention.  Please respond with `!setnickname` and follow the instructions before verifying.");
                    return;
                }

                discordMember.GetNicknameConventionParts(out string CorpCode, out string InGameUserName, out string CompanyCode);

                var isUserNameCorrectMsg = await ctx.RespondAsync($"Is your in-game username **exactly** `{InGameUserName}` (_including_ CaSe)?  Respond with `Yes` or `No`.");
                var yesNoMsg = await ctx.Message.GetNextMessageAsync();
                if (yesNoMsg.TimedOut) return;

                var yesNoContent = yesNoMsg.Result.Content.ToUpper().Trim();
                if (yesNoContent != "Y" && !yesNoContent.StartsWith("YE") && !yesNoContent.StartsWith("YA"))
                {
                    var exactPromptMsg = await yesNoMsg.Result.RespondAsync("Please type your in-game username **ExAcTlY** as it appears in game:");
                    var exactPromptResponseMsg = await ctx.Message.GetNextMessageAsync();
                    if (exactPromptResponseMsg.TimedOut) return;

                    InGameUserName = exactPromptResponseMsg.Result.Content;
                }

                Guid guid = Guid.NewGuid();
                string verificationString = $"VERIFICATION: {guid.ToString().ToUpper()}";

                string verificationMsgContent =
                    "To verify your account, you will need to:\n" +
                    " 1. Open APEX\n" +
                    " 2. Open a new buffer and type in `COMP UFO`\n" +
                    " 3. Join the chat channel\n" +
                    " 3. Paste the following into the UFO chat channel:\n" +
                    $"```{verificationString}```\n\n" +
                    "For the next three minutes, I'll be checking to see if you've done so. I'll let you know if I find your verification message.";
                var verificationMsg = await ctx.SendDMResponse(verificationMsgContent);

                bool bVerified = false;
                for (int VerifyCheckRetryCount = 0; VerifyCheckRetryCount < 30 && !bVerified; ++VerifyCheckRetryCount)
                {
                    // Pause for 10 seconds
                    await Task.Delay(10000);

                    var messages = await Globals.DataRetrieval.GetChatMessagesForUserInUFOChannel(InGameUserName);
                    if (messages != null)
                    {
                        foreach (var message in messages)
                        {
                            if (message.MessageText != null && message.MessageText.ToUpper().Trim() == verificationString)
                            {
                                bVerified = true;
                                break;
                            }
                        }
                    }
                }

                if (bVerified)
                {
                    using (var ufoTransaction = new UFODbTransaction())
                    {
                        var mapping = ufoTransaction.DB.InGameUserToDiscordUserModels.Where(map => map.InGameUsername.ToUpper() == InGameUserName.ToUpper()).FirstOrDefault();
                        bool bAdd = (mapping == null);
                        if (mapping == null)
                        {
                            mapping = new InGameUserToDiscordUserModel();
                        }

                        mapping.InGameUsername = InGameUserName;
                        mapping.DiscordId = discordMember.Id;
                        mapping.DiscordUsername = discordMember.Username;

                        if (bAdd)
                        {
                            ufoTransaction.DB.InGameUserToDiscordUserModels.Add(mapping);
                        }

                        var discordUser = ufoTransaction.DB.DiscordUsers.FirstOrDefault(du => du.DiscordId == discordMember.Id);
                        discordUser.Verified = true;
                        await ufoTransaction.DB.DiscordUsers.Upsert(discordUser)
                            .On(du => new { du.DiscordId })
                            .RunAsync();

                        var guild = await ctx.GetGuildAsync();
                        DiscordRole verifiedRole = guild.GetVerifiedRole();

                        if (verifiedRole != null)
                        {
                            await discordMember.GrantRoleAsync(verifiedRole, "Verified user");
                        }

                        await ctx.SendDMResponse("You have been successfully verified.");

                        await ufoTransaction.DB.SaveChangesAsync();
                    }
                }
            }
        }
    }
}
