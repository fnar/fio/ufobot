using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text.RegularExpressions;
using System.Timers;
using System.Threading.Tasks;

using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.EventArgs;

using Microsoft.Extensions.Configuration;

namespace UFOBot.Events
{
    class StatusDefinition
    {
        public string ActivityType
        {
            get; set;
        }
        public string StatusText
        {
            get; set;
        }
    }

    public static class ActivityEvent
    {
        private static ConfigurationRoot StatusMessagesConfig = null;
        private static ConfigurationSection StatusSection = null;

        private static bool Initialized = false;
        private static List<string> Usernames = new List<string>();
        private static List<string> Planets = new List<string>();
        private static List<string> Materials = new List<string>();
        private static List<string> Corporations = new List<string>();
        private static List<StatusDefinition> Statuses = new List<StatusDefinition>();

        private static Random Rnd = new Random();
        private static int ActivityUpdateTimerFrequency =
#if DEBUG
        (int)new TimeSpan(hours: 0, minutes: 0, seconds: 5).TotalMilliseconds;
#else
        (int)new TimeSpan(hours: 0, minutes: 5, seconds: 0).TotalMilliseconds;
#endif
        private static Timer ActivityUpdateTimer = new Timer(ActivityUpdateTimerFrequency);
        private static DiscordClient ActivityEventClient = null;

        public static async Task Init(DiscordClient client)
        {
            client.Ready += Ready;
            try
            {
                StatusMessagesConfig = (ConfigurationRoot)new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("status-messages.json")
                    .Build();

                StatusSection = (ConfigurationSection)StatusMessagesConfig.GetSection("Status");

                Usernames = StatusSection.GetSection("Usernames").Get<List<string>>();

                var SettledPlanetDefs = await Globals.DataRetrieval.GetSettledPlanetDefs();
                if (SettledPlanetDefs != null && SettledPlanetDefs.Count > 2)
                {
                    Planets = SettledPlanetDefs.Select(spd => spd.Name).ToList();
                }
                else
                {
                    // Fallback
                    Planets = StatusSection.GetSection("Planets").Get<List<string>>();
                }

                var MaterialDefs = await Globals.DataRetrieval.GetMaterialDefs();
                if (MaterialDefs != null && MaterialDefs.Count > 2)
                {
                    Materials = MaterialDefs.Select(s => s.Ticker).ToList();
                }
                else
                {
                    // Fallback
                    Materials = StatusSection.GetSection("Materials").Get<List<string>>();
                }

                Corporations = StatusSection.GetSection("Corporations").Get<List<string>>();
                Statuses = StatusSection.GetSection("Statuses").Get<List<StatusDefinition>>();
                Initialized = true;
            }
            catch
            {
                // Soft-fail
            }
        }

        private static async Task Ready(DiscordClient client, ReadyEventArgs e)
        {
            if (!Initialized)
            {
                return;
            }

            ActivityEventClient = client;

            ActivityUpdateTimer.Elapsed += OnActivityUpdateTimer;
            ActivityUpdateTimer.AutoReset = true;
            ActivityUpdateTimer.Enabled = true;
            await SetRandomStatusAsync();
        }

        private static void OnActivityUpdateTimer(object souce, ElapsedEventArgs e)
        {
            _ = SetRandomStatusAsync();
        }

        private const string UsernameSpecifier = "%username%";
        private static Regex UsernameReplaceRegex = new Regex(Regex.Escape(UsernameSpecifier), RegexOptions.Compiled);

        private const string PlanetSpecifier = "%planet%";
        private static Regex PlanetReplaceRegex = new Regex(Regex.Escape(PlanetSpecifier), RegexOptions.Compiled);

        private const string MaterialSpecifier = "%material%";
        private static Regex MaterialReplaceRegex = new Regex(Regex.Escape(MaterialSpecifier), RegexOptions.Compiled);

        private const string CorporationSpecifier = "%corporation%";
        private static Regex CorporationReplaceRegex = new Regex(Regex.Escape(CorporationSpecifier), RegexOptions.Compiled);

        private static async Task SetRandomStatusAsync()
        {
            var workingUsernames = new List<string>(Usernames);
            var workingPlanets = new List<string>(Planets);
            var workingMaterials = new List<string>(Materials);
            var workingCorporations = new List<string>(Corporations);

            int RandomStatusIdx = Rnd.Next(Statuses.Count);

            var ActivityTypeStr = Statuses[RandomStatusIdx].ActivityType;
            var StatusText = Statuses[RandomStatusIdx].StatusText;
            while (StatusText.Contains(UsernameSpecifier))
            {
                int RandomUsernameIdx = Rnd.Next(workingUsernames.Count);
                StatusText = UsernameReplaceRegex.Replace(StatusText, workingUsernames[RandomUsernameIdx], 1);
                workingUsernames.RemoveAt(RandomUsernameIdx);
            }

            while (StatusText.Contains(PlanetSpecifier))
            {
                int RandomPlanetIdx = Rnd.Next(workingPlanets.Count);
                StatusText = PlanetReplaceRegex.Replace(StatusText, workingPlanets[RandomPlanetIdx], 1);
                workingPlanets.RemoveAt(RandomPlanetIdx);
            }

            while (StatusText.Contains(MaterialSpecifier))
            {
                int RandomMaterialIdx = Rnd.Next(workingMaterials.Count);
                StatusText = MaterialReplaceRegex.Replace(StatusText, workingMaterials[RandomMaterialIdx], 1);
                workingMaterials.RemoveAt(RandomMaterialIdx);
            }

            while (StatusText.Contains(CorporationSpecifier))
            {
                int RandomCorporationIdx = Rnd.Next(workingCorporations.Count);
                StatusText = CorporationReplaceRegex.Replace(StatusText, workingCorporations[RandomCorporationIdx], 1);
                workingCorporations.RemoveAt(RandomCorporationIdx);
            }

            ActivityType StatusActivityType;
            switch(ActivityTypeStr)
            {
                case "Playing":
                    StatusActivityType = ActivityType.Playing;
                    break;
                case "Streaming":
                    StatusActivityType = ActivityType.Streaming;
                    break;
                case "ListeningTo":
                    StatusActivityType = ActivityType.ListeningTo;
                    break;
                case "Watching":
                    StatusActivityType = ActivityType.Watching;
                    break;
                case "Competing":
                    StatusActivityType = ActivityType.Competing;
                    break;
                default:
                    return;
            }

            await ActivityEventClient.UpdateStatusAsync(new DiscordActivity(StatusText, StatusActivityType));
        }
    }
}