﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.EventArgs;
using Microsoft.EntityFrameworkCore;
using UFOBot.Database;
using UFOBot.Database.Models;
using UFOBot.Extensions;

namespace UFOBot.Events
{
    public static class MessageEvents
    {
        private static int MessageEventsUpdateTimerFrequency = (int)new TimeSpan(hours: 1, minutes: 0, seconds: 0).TotalMilliseconds;
        private static Timer MessageEventsUpdateTimer = new Timer(MessageEventsUpdateTimerFrequency);

        public static void Init(DiscordClient client)
        {
            client.Ready += Ready;
            client.MessageCreated += MessageCreated;
            client.MessageUpdated += MessegeUpdated;
            client.MessageDeleted += MessageDeleted;
            client.GuildMemberAdded += MemberAdded;
            client.GuildMemberUpdated += MemberUpdated;
            client.GuildMemberRemoved += MemberRemoved;

            client.GuildDownloadCompleted += (s, e) =>
            {
                // This task might take a while; so let's not pause the bot
                // nor get the D#+ library to whine.
                _ = Task.Run(async () =>
                {
                    await GuildDownloadCompletedEvent(s, e);
                });
                return Task.CompletedTask;
            };
        }

        private static async Task GuildDownloadCompletedEvent(DiscordClient client, GuildDownloadCompletedEventArgs evt)
        {
            var guild = evt.Guilds.Values.First();

            _ = Task.Run(async () =>
            {
                await PopulateDiscordUsersDb(guild);
                await CheckForDiscordUserConflicts(guild, null);
            });

            await Task.FromResult(0);
        }

        private static async Task Ready(DiscordClient client, ReadyEventArgs e)
        {
            MessageEventsUpdateTimer.Elapsed += OnMessageEventsUpdateTimer;
            MessageEventsUpdateTimer.AutoReset = true;
            MessageEventsUpdateTimer.Enabled = true;

            await Task.FromResult(0);
        }

        private static TimeSpan OneHundredDays = TimeSpan.FromDays(100);
        private static void OnMessageEventsUpdateTimer(object souce, ElapsedEventArgs e)
        {
            _ = Task.Run(async () =>
            {
                using (var ufoTransaction = new UFODbTransaction())
                {
                    var oneHundredDaysAgo = DateTime.UtcNow.Subtract(OneHundredDays);
                    var messageModelsToDelete = ufoTransaction.DB.DiscordMessageModels.Where(dmm => dmm.TimestampUtc < oneHundredDaysAgo);
                    ufoTransaction.DB.DiscordMessageModels.RemoveRange(messageModelsToDelete);
                    await ufoTransaction.DB.SaveChangesAsync();
                }
            });
        }

        private static async Task MessageCreated(DiscordClient client, MessageCreateEventArgs e)
        {
            if (e.Guild == null) return;

            _ = Task.Run(async () =>
            {
                using (var ufoTransaction = new UFODbTransaction())
                {
                    var messageModel = new DiscordMessageModel(e.Message);
                    ufoTransaction.DB.DiscordMessageModels.Add(messageModel);

                    ulong AuthorId = e.Message.Author.Id;
                    var discordMember = await e.Guild.GetMemberAsync(AuthorId);
                    if (!e.Message.Channel.IsManagedChannel() && !discordMember.IsBot && !discordMember.IsOwner)
                    {
                        var discordUserModel = await ufoTransaction.DB.DiscordUsers.FirstOrDefaultAsync(du => du.DiscordId == AuthorId);
                        if (discordUserModel != null && discordUserModel.ShouldComplain())
                        {
                            discordUserModel.SetNewComplain();

                            await ufoTransaction.DB.DiscordUsers.Upsert(discordUserModel)
                                .On(du => new { du.DiscordId })
                                .RunAsync();

                            await NotifyUserOfBadNickname(discordMember);
                        }
                    }

                    await ufoTransaction.DB.SaveChangesAsync();
                }
            });


            await Task.FromResult(0);
        }

        private static async Task MessegeUpdated(DiscordClient client, MessageUpdateEventArgs e)
        {
            if (e.Guild == null) return;

            _ = Task.Run(async () =>
            {
                using (var ufoTransaction = new UFODbTransaction())
                {
                    var messageModel = ufoTransaction.DB.DiscordMessageModels.Where(dmm => dmm.MessageId == e.Message.Id).FirstOrDefault();
                    if (messageModel != null)
                    {
                        messageModel.Edits.Add(new DiscordEditMessageModel(e.Message));
                        await ufoTransaction.DB.SaveChangesAsync();
                    }
                }
            });

            await Task.FromResult(0);
        }

        private static async Task MessageDeleted(DiscordClient client, MessageDeleteEventArgs e)
        {
            DateTimeOffset deleteTimestamp = DateTimeOffset.UtcNow;
            if (e.Guild == null) return;

            _ = Task.Run(async () =>
            {
                // The audit log can be delayed a bit, so give it 3s
                await Task.Delay(3000);

                var auditLogEntries = (await e.Guild.GetAuditLogsAsync(action_type: AuditLogActionType.MessageDelete)).Select(ale => (Discord​Audit​Log​Message​Entry)ale).ToList();
                var closestAuditLogEntry = auditLogEntries.OrderBy(t => Math.Abs(deleteTimestamp.Ticks - t.CreationTimestamp.Ticks)).FirstOrDefault();

                using (var ufoTransaction = new UFODbTransaction())
                {
                    var messageModel = ufoTransaction.DB.DiscordMessageModels.Where(dmm => dmm.MessageId == e.Message.Id).FirstOrDefault();
                    if (messageModel != null)
                    {
                        messageModel.Deleted = true;
                        if (closestAuditLogEntry != null && closestAuditLogEntry.Channel.Id == messageModel.ChannelId)
                        {
                            TimeSpan threshold = TimeSpan.FromMilliseconds(200.0); // 200ms threshold
                            TimeSpan diff = (closestAuditLogEntry.CreationTimestamp - deleteTimestamp).Duration();
                            if (diff < threshold)
                            {
                                messageModel.DeletedByUserId = closestAuditLogEntry.UserResponsible.Id;
                                messageModel.DeletedByUsername = closestAuditLogEntry.UserResponsible.Username;
                            }
                            else
                            {
                                messageModel.DeletedByUserId = messageModel.AuthorId;
                                messageModel.DeletedByUsername = messageModel.AuthorUsername;
                            }
                        }
                        else
                        {
                            messageModel.DeletedByUserId = messageModel.AuthorId;
                            messageModel.DeletedByUsername = messageModel.AuthorUsername;
                        }

                        if (messageModel.AuthorId != messageModel.DeletedByUserId)
                        {
                            // Emit message
                            var auditChannel = e.Guild.GetAuditLogChannel();
                            string auditMessage = $"**{messageModel.DeletedByUsername} deleted {messageModel.AuthorUsername}'s message**\n" +
                                                  $"https://discord.com/channels/{e.Guild.Id}/{messageModel.ChannelId}/{messageModel.MessageId}";
                            await auditChannel.SendMessageAsync(auditMessage);
                        }

                        messageModel.DeletedTimestampUtc = deleteTimestamp.DateTime;
                        await ufoTransaction.DB.SaveChangesAsync();
                    }
                }
            });

            await Task.FromResult(0);
        }

        private static DiscordUserModel GetDiscordUserModelFromMember(DiscordMember member, bool bUseOverride = false, string overrideNickname = null)
        {
            var discordUser = new DiscordUserModel();
            discordUser.DiscordId = member.Id;
            discordUser.DiscordUsername = member.Username;
            discordUser.DiscordDiscriminator = member.Discriminator;

            string CorporationCode, Username, CompanyCode;
            if (bUseOverride)
            {
                discordUser.MatchesConvention = overrideNickname.GetNicknameConventionParts(out CorporationCode, out Username, out CompanyCode);
            }
            else
            {
                discordUser.MatchesConvention = member.GetNicknameConventionParts(out CorporationCode, out Username, out CompanyCode);
            }

            if (discordUser.MatchesConvention)
            {
                discordUser.Verified = member.IsVerified();
                discordUser.CorporationCode = CorporationCode;
                discordUser.CompanyCode = CompanyCode;
                discordUser.NextMatchesConventionComplaint = DateTime.MaxValue;
            }
            else
            {
                discordUser.NextMatchesConventionComplaint = DateTime.MinValue;
            }

            return discordUser;
        }

        public static async Task PopulateDiscordUsersDb(DiscordGuild guild)
        {
            var guildMembers = await guild.GetAllMembersAsync();
            guildMembers = guildMembers.Where(gm => !gm.IsBot && !gm.IsOwner).ToList();

            using (var ufoTransaction = new UFODbTransaction())
            {
                var discordUsers = new List<DiscordUserModel>();
                foreach (var guildMember in guildMembers)
                {
                    var discordUser = GetDiscordUserModelFromMember(guildMember);
                    discordUsers.Add(discordUser);
                }

                await ufoTransaction.DB.DiscordUsers.UpsertRange(discordUsers)
                    .On(du => new { du.DiscordId })
                    .WhenMatched((existingUser, newUser) => new DiscordUserModel
                    {
                        CorporationCode = newUser.CorporationCode,
                        CompanyCode = newUser.CompanyCode,
                        MatchesConvention = newUser.MatchesConvention,
                        Verified = newUser.Verified,
                        NextMatchesConventionComplaint = newUser.NextMatchesConventionComplaint,
                    })
                    .RunAsync();

                await ufoTransaction.DB.SaveChangesAsync();
            }
        }

        public static async Task CheckForDiscordUserConflicts(DiscordGuild guild, DiscordUserModel discordUserModel)
        {
            if (discordUserModel != null && !discordUserModel.MatchesConvention)
                return;

            var AuditLogMessages = new List<String>(0);

            using (var ufoTransaction = new UFODbTransaction())
            {
                List<DiscordUserModel> duplicates;
                if (discordUserModel != null)
                {
                    duplicates = await ufoTransaction.DB.DiscordUsers
                         .Where(du => du.CompanyCode != null && du.CompanyCode != "" && du.CompanyCode == discordUserModel.CompanyCode)
                         .ToListAsync();

                    if (duplicates.Count == 1)
                    {
                        duplicates.Clear();
                    }
                }
                else
                {
                    duplicates = await
                        (from du in ufoTransaction.DB.DiscordUsers
                         where du.CompanyCode != null && du.CompanyCode != ""
                         group du by du.CompanyCode into g
                         where g.Count() > 1
                         select g.Key)
                        .SelectMany(k => ufoTransaction.DB.DiscordUsers.Where(t => t.CompanyCode == k))
                        .ToListAsync();
                }

                List<DiscordUserModel> modifiedDiscordUserModels = new List<DiscordUserModel>();

                var duplicateDictionary = duplicates.GroupBy(dup => dup.CompanyCode).ToDictionary(x => x.Key, g => g.ToList());
                foreach (var kvp in duplicateDictionary)
                {
                    var CompanyCode = kvp.Key;
                    var DiscordUsers = kvp.Value;

                    var unverifiedUsers = DiscordUsers.Where(us => us.Verified == false).ToList();
                    var verifiedUsers = DiscordUsers.Where(du => !unverifiedUsers.Any(uu => du.DiscordId == uu.DiscordId)).ToList();

                    if (verifiedUsers.Count > 1)
                    {
                        var sb = new StringBuilder();
                        sb.Append($"Detected duplicate verified users for company code '{CompanyCode}': ");
                        foreach (var verifiedUser in verifiedUsers)
                        {
                            if (verifiedUser.DiscordDiscriminator == null || verifiedUser.DiscordDiscriminator == "0")
                            {
                                sb.Append($"{verifiedUser.DiscordUsername} ");
                            }
                            else
                            {
                                sb.Append($"{verifiedUser.DiscordUsername}#{verifiedUser.DiscordDiscriminator} ");
                            }
                            
                        }
                        AuditLogMessages.Add(sb.ToString().Trim());
                    }

                    foreach (var unverifiedUser in unverifiedUsers)
                    {
                        unverifiedUser.CompanyCode = null;
                        unverifiedUser.CorporationCode = null;
                        unverifiedUser.MatchesConvention = false;
                        unverifiedUser.SetNewComplain();
                    }

                    if (unverifiedUsers.Count > 0)
                    {
                        var s = unverifiedUsers.Count > 1 ? "s" : "";
                        AuditLogMessages.Add($"Cleared nickname{s} for {unverifiedUsers.Count} unverified user{s} using company code '{CompanyCode}' due to conflict");
                    }

                    await ufoTransaction.DB.DiscordUsers.UpsertRange(unverifiedUsers)
                        .On(du => new { du.DiscordId })
                        .WhenMatched((existingUser, newUser) => new DiscordUserModel
                        {
                            CorporationCode = newUser.CorporationCode,
                            CompanyCode = newUser.CompanyCode,
                            MatchesConvention = newUser.MatchesConvention,
                            Verified = newUser.Verified,
                            NextMatchesConventionComplaint = newUser.NextMatchesConventionComplaint,
                        })
                        .RunAsync();

                    foreach (var unverifiedUser in unverifiedUsers)
                    {
                        var discordMember = await guild.GetMemberAsync(unverifiedUser.DiscordId);
                        await RemoveNicknameAndNotifyUser(discordMember);
                        await Task.Delay(500);
                    }
                }
            }

            await guild.EmitMessagesToAuditLog("User Conflicts", AuditLogMessages);
        }

        public static async Task MemberAdded(DiscordClient client, GuildMemberAddEventArgs e)
        {
            using (var ufoTransaction = new UFODbTransaction())
            {
                var discordUser = GetDiscordUserModelFromMember(e.Member);
                ufoTransaction.DB.DiscordUsers.Add(discordUser);
                await ufoTransaction.DB.SaveChangesAsync();
            }
        }

        public static async Task MemberUpdated(DiscordClient client, GuildMemberUpdateEventArgs e)
        {
            using (var ufoTransaction = new UFODbTransaction())
            {
                var guild = e.Member.Guild;
                var newNickname = e.NicknameAfter;

                var discordUserBefore = await ufoTransaction.DB.DiscordUsers.FirstOrDefaultAsync(du => du.DiscordId == e.Member.Id);
                if (discordUserBefore == null)
                {
                    return;
                }

                var discordUser = GetDiscordUserModelFromMember(e.Member, true, e.NicknameAfter);

                bool bNeedToNotify = discordUserBefore.ShouldComplain();
                bool bMatchesConventionBefore = discordUserBefore.MatchesConvention;
                bool bMatchesConventionNow = discordUser.MatchesConvention;

                bool bNicknameRemainedValid = bMatchesConventionBefore && bMatchesConventionNow;
                bool bNicknameRemainedInvalid = !bMatchesConventionBefore && !bMatchesConventionNow;
                bool bNicknameBecameValid = !bMatchesConventionBefore && bMatchesConventionNow;
                bool bNicknameBecameInvalid = bMatchesConventionBefore && !bMatchesConventionNow;

                if (bNicknameRemainedValid || bNicknameBecameValid)
                {
                    discordUser.SetNoComplain();
                }
                else if (bNicknameBecameInvalid)
                {
                    discordUser.NextMatchesConventionComplaint = DateTime.MinValue;
                }

                DiscordMember memberToRemoveNickname = null;
                List<DiscordUserModel> modifiedDiscordUserModels = new List<DiscordUserModel>();

                if (discordUser.MatchesConvention)
                {
                    // Check for an existing user that conflicts
                    var existingUser = await ufoTransaction.DB.DiscordUsers.FirstOrDefaultAsync(du => du.CompanyCode == discordUser.CompanyCode && du.DiscordId != discordUser.DiscordId);
                    if (existingUser != null)
                    {
                        // The already existing verified user with this company code wins.  This user gets renamed.
                        memberToRemoveNickname = e.Member;
                        discordUser.SetNewComplain();
                    }
                }

                // Treat the modified discordUser as always having been
                modifiedDiscordUserModels.Add(discordUser);

                await ufoTransaction.DB.DiscordUsers.UpsertRange(modifiedDiscordUserModels)
                    .On(du => new { du.DiscordId })
                    .WhenMatched((existingUser, newUser) => new DiscordUserModel
                    {
                        CorporationCode = newUser.CorporationCode,
                        CompanyCode = newUser.CompanyCode,
                        MatchesConvention = newUser.MatchesConvention,
                        Verified = newUser.Verified,
                        NextMatchesConventionComplaint = newUser.NextMatchesConventionComplaint,
                    })
                    .RunAsync();

                if (memberToRemoveNickname != null)
                {
                    await RemoveNicknameAndNotifyUser(memberToRemoveNickname);
                }

                await ufoTransaction.DB.SaveChangesAsync();
            }
        }

        public static async Task MemberRemoved(DiscordClient client, GuildMemberRemoveEventArgs e)
        {
            using (var ufoTransaction = new UFODbTransaction())
            {
                var discordUser = await ufoTransaction.DB.DiscordUsers.FirstOrDefaultAsync(du => du.DiscordId == e.Member.Id);
                ufoTransaction.DB.DiscordUsers.Remove(discordUser);
                await ufoTransaction.DB.SaveChangesAsync();
            }
        }

        const string RemoveNicknameNotify =
            "**Your nickname was removed due to you using the same company code as another user**. Please set your company code correctly.\n" +
            "If it is set correctly, run the verification process by messaging me `!verify` here and then set your nickname by sending me `!setnickname`.\n" +
            "If issues persist, it means that two verified users claim to have the same company code--contact a moderator.";

        public static async Task RemoveNicknameAndNotifyUser(DiscordMember member)
        {
            await member.ModifyAsync(x =>
            {
                x.Nickname = null;
                x.AuditLogReason = "Removed nickname due to conflict";
            });

            await member.SendDM(RemoveNicknameNotify, true);
        }

        public static async Task NotifyUserOfBadNickname(DiscordMember member)
        {
            await member.SendDM(Commands.CommandConstants.NicknameConventionWarning, true);
        }
    }
}
