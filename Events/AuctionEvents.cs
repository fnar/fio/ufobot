﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;

using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.EventArgs;
using Microsoft.EntityFrameworkCore;
using UFOBot.Commands;
using UFOBot.Database;
using UFOBot.Database.Models;
using UFOBot.Extensions;
using UFOBot.Utils;

namespace UFOBot.Events
{
    public static class AuctionEvents
    {
        public static void Init(DiscordClient client)
        {
            client.GuildDownloadCompleted += (s, e) =>
            {
                _ = Task.Run(async () =>
                {
                    await GuildDownloadCompletedEvent(s, e);
                });

                return Task.CompletedTask;
            };
        }

        private static DiscordClient discordClient = null;
        private static async Task GuildDownloadCompletedEvent(DiscordClient client, GuildDownloadCompletedEventArgs evt)
        {
            discordClient = client;
            var guild = evt.Guilds.Values.First();

            _ = Task.Run(async () =>
            {
                await StartAuctionEventTimers();
            });

            await Task.FromResult(0);
        }

        public static async Task StartAuctionEventTimers()
        {
            var activeAuctions = await AuctionCommands.GetActiveAuctionsAsync();
            foreach (var auction in activeAuctions)
            {
                AddAuctionEvent(auction);
            }
        }

        public static async Task ConditionalCancelAuction(string auctionId, AuctionEventType eventType)
        {
            using (var ufoTransaction = new UFODbTransaction())
            {
                Auction auction = (eventType == AuctionEventType.BiddingEnded) ?
                    await ufoTransaction.DB.Auctions.Include(a => a.HashedBids).FirstOrDefaultAsync(a => a.AuctionId == auctionId) :
                    await ufoTransaction.DB.Auctions.Include(a => a.HashedBids).Include(a => a.UnhashedBids).FirstOrDefaultAsync(a => a.AuctionId == auctionId);

                if (eventType == AuctionEventType.BiddingEnded && auction.AuctionHadNoBids)
                {
                    auction.AuctionCancelled = true;
                    StopAuctionEvent(auctionId);
                    await ufoTransaction.DB.SaveChangesAsync();
                }
                else if (eventType == AuctionEventType.VerificationEnded && auction.AuctionHadNoVerifications)
                {
                    auction.AuctionCancelled = true;
                    StopAuctionEvent(auctionId);
                    await ufoTransaction.DB.SaveChangesAsync();
                }
                else if (eventType == AuctionEventType.VerificationEnded && auction.HashedBids.Count == auction.UnhashedBids.Count)
                {
                    auction.VerificationEndTime = DateTime.UtcNow;
                    StopAuctionEvent(auctionId);
                    await ufoTransaction.DB.SaveChangesAsync();
                }
            }
        }

        public static async Task NotifyAuctionStart(Auction auction)
        {
            AddAuctionEvent(auction);

            var discordChannel = discordClient.GetGuild().GetChannel(auction.ChannelId);

            var AuctionStartedEmbed = new DiscordEmbedBuilder()
                .WithColor(DiscordColor.Green)
                .WithTitle($"Auction {auction.AuctionId} has started")
                .AddField("Bidding End Time", $"<t:{auction.BiddingEndTime.ToEpochSeconds()}>")
                .AddField("Verification End Time", $"<t:{auction.VerificationEndTime.ToEpochSeconds()}>")
                .AddField("To bid", $"DM UFOBot `!bidauction {auction.AuctionId}`")
                .WithDescription(auction.Description);

            await discordChannel.SendMessageAsync(AuctionStartedEmbed);
        }

        public static async Task NotifyChannelOfAuctionEvent(string auctionId, AuctionEventType eventType)
        {
            Auction auction;
            using (var DB = new UFODbContext())
            {
                auction = (eventType == AuctionEventType.BiddingEnded) ?
                    await DB.Auctions.Include(a => a.HashedBids).FirstOrDefaultAsync(a => a.AuctionId == auctionId) :
                    await DB.Auctions.Include(a => a.HashedBids).Include(a => a.UnhashedBids).FirstOrDefaultAsync(a => a.AuctionId == auctionId);
            }

            var embedToSend = new DiscordEmbedBuilder();

            if (eventType == AuctionEventType.BiddingEnded)
            {
                embedToSend
                    .WithColor(DiscordColor.Yellow)
                    .WithTitle($"Bidding for {auction.AuctionId} is now closed.");
                if (auction.HashedBids.Count == 0)
                {
                    embedToSend.WithDescription("There were no bids for this auction.\n**Auction is cancelled.**");
                }
                else
                {
                    string VerificationPhaseMessage =
                        "The verification phase of this auction has begun.\n" +
                        $"Verification will end at <t:{auction.VerificationEndTime.ToEpochSeconds()}>.\n" +
                        "The following users must verify their bids:";
                    embedToSend.WithDescription(VerificationPhaseMessage);
                    foreach (var hashedBid in auction.HashedBids)
                    {
                        embedToSend.AddField(hashedBid.InGameUserName, hashedBid.SHA256Hash);
                    }
                }
            }
            else
            {
                embedToSend
                    .WithColor(DiscordColor.Red)
                    .WithTitle($"Verification for {auction.AuctionId} is now closed");
                if (auction.UnhashedBids.Count == 0)
                {
                    embedToSend.WithDescription("There were no verifications for this auction.\n**Auction is cancelled.**");
                }
                else
                {
                    embedToSend.WithDescription("Results of the auction:");
                    foreach (var unhashedBid in auction.UnhashedBids)
                    {
                        embedToSend.AddField(unhashedBid.InGameUserName, unhashedBid.UnhashedString);
                    }
                }
            }

            var discordChannel = discordClient.GetGuild().GetChannel(auction.ChannelId);
            await discordChannel.SendMessageAsync(embedToSend);
        }

        public static async Task NotifyAuctionBidAdded(Auction auction, HashedBid bid)
        {
            var linesToSend = new List<string>();
            linesToSend.Add($"**Auction {auction.AuctionId}:** {bid.InGameUserName} submitted bid.");

            var discordChannel = discordClient.GetGuild().GetChannel(auction.ChannelId);
            await discordChannel.SendMessageLinesAsync(linesToSend);
        }

        public static async Task NotifyAuctionBidRemoved(Auction auction, HashedBid bid)
        {
            var linesToSend = new List<string>();
            linesToSend.Add($"**Auction {auction.AuctionId}:** {bid.InGameUserName} removed bid.");

            var discordChannel = discordClient.GetGuild().GetChannel(auction.ChannelId);
            await discordChannel.SendMessageLinesAsync(linesToSend);
        }

        public static async Task NotifyAuctionVerificationAdded(Auction auction, UnhashedBid bid)
        {
            var linesToSend = new List<string>();
            linesToSend.Add($"**Auction {auction.AuctionId}:** {bid.InGameUserName} verified bid.");
            linesToSend.Add($"\tBid Text: {bid.UnhashedString}");

            var discordChannel = discordClient.GetGuild().GetChannel(auction.ChannelId);
            await discordChannel.SendMessageLinesAsync(linesToSend);
        }

        public static async Task NotifyUsersOfAuctionEvent(string auctionId, AuctionEventType eventType)
        {
            Auction auction;
            using (var DB = new UFODbContext())
            {
                auction = (eventType == AuctionEventType.BiddingEnded) ?
                    await DB.Auctions.Include(a => a.HashedBids).FirstOrDefaultAsync(a => a.AuctionId == auctionId) :
                    await DB.Auctions.Include(a => a.HashedBids).Include(a => a.UnhashedBids).FirstOrDefaultAsync(a => a.AuctionId == auctionId);
            }

            var guild = discordClient.GetGuild();
            if (eventType == AuctionEventType.BiddingEnded)
            {
                foreach (var hashedBid in auction.HashedBids)
                {
                    var discordMember = await guild.GetMemberAsync(hashedBid.DiscordUserId);

                    string BiddingPhaseFinishedMessage =
                        $"Bidding phase for {auction.AuctionId} has finished.\n" +
                        $"Auction Summary: {auction.Summary}\n" +
                        $"You now must verify your bid by issuing the command `!verifybid {auction.AuctionId} <InputString>`";
                    await discordMember.SendDM(BiddingPhaseFinishedMessage);
                    await Task.Delay(500);
                }
            }
            else
            {
                foreach (var unhashedBid in auction.UnhashedBids)
                {
                    var discordMember = await guild.GetMemberAsync(unhashedBid.DiscordUserId);
                    var discordChannel = guild.GetChannel(auction.ChannelId);
                    await discordMember.SendDM($"Verification phase for {auction.AuctionId} has finished. See results in `#{discordChannel.Name}`.");
                    await Task.Delay(500);
                }
            }
        }


        public enum AuctionEventType
        {
            BiddingEnded,
            VerificationEnded
        }

        public class AuctionTimer : Timer
        {
            public bool EventTriggered
            {
                get;
                private set;
            } = false;

            public Auction auction
            {
                get;
                private set;
            }

            public AuctionEventType eventType
            {
                get;
                private set;
            }

            public AuctionTimer(Auction auction, AuctionEventType eventType)
            {
                this.auction = auction;
                this.eventType = eventType;

                var eventTime = (eventType == AuctionEventType.BiddingEnded) ? auction.BiddingEndTime : auction.VerificationEndTime;
                var millisecondsToEvent = eventTime.Subtract(DateTime.UtcNow).TotalMilliseconds;
                if (millisecondsToEvent <= 0.0)
                {
                    OnAuctionEventElapsed(this, null);
                    EventTriggered = true;
                }
                else
                {
                    Interval = millisecondsToEvent;

                    AutoReset = false;
                    Elapsed += OnAuctionEventElapsed;
                    Start();
                }
            }

            public static void OnAuctionEventElapsed(object sender, ElapsedEventArgs e)
            {
                var auctionTimer = sender as AuctionTimer;
                var auction = auctionTimer.auction;
                var eventType = auctionTimer.eventType;

                if (eventType == AuctionEventType.BiddingEnded)
                {
                    BiddingEndedTimers.Remove(auctionTimer);
                    VerificationEndedTimers.Add(new AuctionTimer(auction, AuctionEventType.VerificationEnded));
                }
                else
                {
                    VerificationEndedTimers.Remove(auctionTimer);
                }

                auctionTimer.EventTriggered = true;

                _ = Task.Run(async () =>
                {
                    await ConditionalCancelAuction(auction.AuctionId, eventType);
                    await NotifyChannelOfAuctionEvent(auction.AuctionId, eventType);
                    await NotifyUsersOfAuctionEvent(auction.AuctionId, eventType);
                });
            }
        }

        private static List<AuctionTimer> BiddingEndedTimers = new List<AuctionTimer>();
        private static List<AuctionTimer> VerificationEndedTimers = new List<AuctionTimer>();

        public static void AddAuctionEvent(Auction auction)
        {
            Trace.Assert(auction != null);
            if (auction.BiddingActive)
            {
                var timer = new AuctionTimer(auction, AuctionEventType.BiddingEnded);
                if (!timer.EventTriggered)
                {
                    BiddingEndedTimers.Add(timer);
                }
            }
            else
            {
                var timer = new AuctionTimer(auction, AuctionEventType.VerificationEnded);
                if (!timer.EventTriggered)
                {
                    VerificationEndedTimers.Add(timer);
                }
            }
        }

        public static void StopAuctionEvent(string auctionId)
        {
            var biddingTimer = BiddingEndedTimers.FirstOrDefault(bet => bet.auction.AuctionId == auctionId);
            var verificationTimer = VerificationEndedTimers.FirstOrDefault(bet => bet.auction.AuctionId == auctionId);

            if (biddingTimer != null)
            {
                biddingTimer.Stop();
                BiddingEndedTimers.Remove(biddingTimer);
            }

            if (verificationTimer != null)
            {
                verificationTimer.Stop();
                VerificationEndedTimers.Remove(verificationTimer);
            }
        }
    }
}
