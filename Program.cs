using System;
using System.Threading.Tasks;
using System.IO;

using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.Interactivity;
using DSharpPlus.Interactivity.Extensions;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using Serilog;

using UFOBot.DataAPI;

namespace UFOBot
{
    static class Globals
    {
        public static ConfigurationRoot configuration;

        // When the time comes, this needs to be switched to the APEX Official mechanism
        public static DataRetrieval DataRetrieval = new DataAPI.FIO.FIODataRetrieval();
    }

    class Program
    {
        private const string UFOBotGuid = "3538eb00-651e-477e-b807-5653b79b9399";
        private static ConfigurationSection discordConfig = null;

        static void Main(string[] args)
        {
            Globals.configuration = (ConfigurationRoot)new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "production"}.json", true)
                .Build();

            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(Globals.configuration)
                .CreateLogger();
            Log.Information("Starting up");

            discordConfig = (ConfigurationSection)Globals.configuration.GetSection("Discord");

            MainAsync(discordConfig).GetAwaiter().GetResult();
        }

        static async Task MainAsync(ConfigurationSection discordConfig)
        {
            using (var gm = new Utils.GlobalMutex(UFOBotGuid))
            {
                using (var db = new Database.UFODbContext())
                {
                    Log.Information("Migrating Database");
                    db.Database.Migrate();
                }

                var discord = new DiscordClient(new DiscordConfiguration()
                {
                    Token = discordConfig["Token"],
                    TokenType = TokenType.Bot,
                    MessageCacheSize = 16384,
                    Intents = DiscordIntents.AllUnprivileged | DiscordIntents.GuildMembers | DiscordIntents.MessageContents
                });

                discord.ChannelCreated += Commands.AdminCommands.ChannelCreatedEvent;
                discord.GuildDownloadCompleted += (s, e) =>
                {
                    // This task might take a while; so let's not pause the bot
                    // nor get the D#+ library to whine.
                    _ = Task.Run(async () =>
                    {
                        await Commands.AdminCommands.GuildDownloadCompletedEvent(s, e);
                    });
                    return Task.CompletedTask;
                };
                discord.Ready += Commands.AdminCommands.Ready;

                await Events.ActivityEvent.Init(discord);
                Events.MessageEvents.Init(discord);
                Events.AuctionEvents.Init(discord);

                // Setup CommandsNext
                var commands = discord.UseCommandsNext(new CommandsNextConfiguration()
                {
                    StringPrefixes = discordConfig.GetSection("Prefixes").Get<string[]>(),
                });

                discord.UseInteractivity(new InteractivityConfiguration()
                {
                    Timeout = TimeSpan.FromSeconds(60)
                });

                commands.RegisterCommands<Commands.AdminCommands>();
                commands.RegisterCommands<Commands.AuctionCommands>();
                commands.RegisterCommands<Commands.ChannelCommands>();
                commands.RegisterCommands<Commands.FactionCommands>();
                commands.RegisterCommands<Commands.GovernorCommands>();
                commands.RegisterCommands<Commands.ServerCommands>();

                await discord.ConnectAsync();

                await Task.Delay(-1);
            }
        }
    }
}
