﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace UFOBot.Migrations
{
    public partial class AddAuctions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Auctions",
                columns: table => new
                {
                    AuctionId = table.Column<string>(type: "TEXT", maxLength: 64, nullable: false),
                    ChannelId = table.Column<ulong>(type: "INTEGER", nullable: false),
                    PlanetNaturalId = table.Column<string>(type: "TEXT", nullable: true),
                    ChannelAuctionIndex = table.Column<int>(type: "INTEGER", nullable: false),
                    CreationTime = table.Column<DateTime>(type: "TEXT", nullable: false),
                    BiddingEndTime = table.Column<DateTime>(type: "TEXT", nullable: false),
                    VerificationEndTime = table.Column<DateTime>(type: "TEXT", nullable: false),
                    AuctionCancelled = table.Column<bool>(type: "INTEGER", nullable: false),
                    Summary = table.Column<string>(type: "TEXT", maxLength: 100, nullable: true),
                    Description = table.Column<string>(type: "TEXT", maxLength: 1500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Auctions", x => x.AuctionId);
                });

            migrationBuilder.CreateTable(
                name: "HashedBids",
                columns: table => new
                {
                    HashedBidId = table.Column<string>(type: "TEXT", maxLength: 128, nullable: false),
                    SHA256Hash = table.Column<string>(type: "TEXT", nullable: true),
                    DiscordUserId = table.Column<ulong>(type: "INTEGER", nullable: false),
                    InGameUserName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    AuctionId = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HashedBids", x => x.HashedBidId);
                    table.ForeignKey(
                        name: "FK_HashedBids_Auctions_AuctionId",
                        column: x => x.AuctionId,
                        principalTable: "Auctions",
                        principalColumn: "AuctionId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UnhashedBids",
                columns: table => new
                {
                    UnhashedBidId = table.Column<string>(type: "TEXT", maxLength: 128, nullable: false),
                    SHA256Hash = table.Column<string>(type: "TEXT", nullable: true),
                    UnhashedString = table.Column<string>(type: "TEXT", maxLength: 350, nullable: true),
                    DiscordUserId = table.Column<ulong>(type: "INTEGER", nullable: false),
                    InGameUserName = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    AuctionId = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UnhashedBids", x => x.UnhashedBidId);
                    table.ForeignKey(
                        name: "FK_UnhashedBids_Auctions_AuctionId",
                        column: x => x.AuctionId,
                        principalTable: "Auctions",
                        principalColumn: "AuctionId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_HashedBids_AuctionId",
                table: "HashedBids",
                column: "AuctionId");

            migrationBuilder.CreateIndex(
                name: "IX_HashedBids_DiscordUserId",
                table: "HashedBids",
                column: "DiscordUserId");

            migrationBuilder.CreateIndex(
                name: "IX_HashedBids_InGameUserName",
                table: "HashedBids",
                column: "InGameUserName");

            migrationBuilder.CreateIndex(
                name: "IX_UnhashedBids_AuctionId",
                table: "UnhashedBids",
                column: "AuctionId");

            migrationBuilder.CreateIndex(
                name: "IX_UnhashedBids_DiscordUserId",
                table: "UnhashedBids",
                column: "DiscordUserId");

            migrationBuilder.CreateIndex(
                name: "IX_UnhashedBids_InGameUserName",
                table: "UnhashedBids",
                column: "InGameUserName");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "HashedBids");

            migrationBuilder.DropTable(
                name: "UnhashedBids");

            migrationBuilder.DropTable(
                name: "Auctions");
        }
    }
}
