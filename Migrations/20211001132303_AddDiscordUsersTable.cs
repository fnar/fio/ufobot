﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace UFOBot.Migrations
{
    public partial class AddDiscordUsersTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DiscordUsers",
                columns: table => new
                {
                    DiscordId = table.Column<ulong>(type: "INTEGER", nullable: false),
                    DiscordUsername = table.Column<string>(type: "TEXT", nullable: true),
                    DiscordDiscriminator = table.Column<string>(type: "TEXT", nullable: true),
                    MatchesConvention = table.Column<bool>(type: "INTEGER", nullable: false),
                    Verified = table.Column<bool>(type: "INTEGER", nullable: false),
                    CorporationCode = table.Column<string>(type: "TEXT", nullable: true),
                    CompanyCode = table.Column<string>(type: "TEXT", nullable: true),
                    NextMatchesConventionComplaint = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DiscordUsers", x => x.DiscordId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DiscordUsers_CompanyCode",
                table: "DiscordUsers",
                column: "CompanyCode");

            migrationBuilder.CreateIndex(
                name: "IX_DiscordUsers_CorporationCode",
                table: "DiscordUsers",
                column: "CorporationCode");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DiscordUsers");
        }
    }
}
