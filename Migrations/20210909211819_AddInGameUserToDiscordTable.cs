﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace UFOBot.Migrations
{
    public partial class AddInGameUserToDiscordTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "InGameUserToDiscordUserModels",
                columns: table => new
                {
                    InGameUserToDiscordUserModelId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    InGameUsername = table.Column<string>(type: "TEXT", nullable: true),
                    DiscordUsername = table.Column<string>(type: "TEXT", nullable: true),
                    DiscordDiscriminator = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InGameUserToDiscordUserModels", x => x.InGameUserToDiscordUserModelId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "InGameUserToDiscordUserModels");
        }
    }
}
