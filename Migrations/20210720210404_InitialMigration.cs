﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace UFOBot.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DiscordMessageModels",
                columns: table => new
                {
                    DiscordMessageModelId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    MessageId = table.Column<ulong>(type: "INTEGER", nullable: false),
                    ChannelId = table.Column<ulong>(type: "INTEGER", nullable: false),
                    ChannelCategory = table.Column<string>(type: "TEXT", nullable: true),
                    ChannelName = table.Column<string>(type: "TEXT", nullable: true),
                    AuthorUsername = table.Column<string>(type: "TEXT", nullable: true),
                    AuthorDiscriminator = table.Column<string>(type: "TEXT", nullable: true),
                    TimestampUtc = table.Column<DateTime>(type: "TEXT", nullable: false),
                    EditedTimestampUtc = table.Column<DateTime>(type: "TEXT", nullable: true),
                    Content = table.Column<string>(type: "TEXT", nullable: true),
                    Deleted = table.Column<bool>(type: "INTEGER", nullable: false),
                    DeletedByUsername = table.Column<string>(type: "TEXT", nullable: true),
                    DeletedByDiscriminator = table.Column<string>(type: "TEXT", nullable: true),
                    DeletedTimestampUtc = table.Column<DateTime>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DiscordMessageModels", x => x.DiscordMessageModelId);
                });

            migrationBuilder.CreateTable(
                name: "DiscordEditMessageModel",
                columns: table => new
                {
                    DiscordEditMessageModelId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    TimestampUtc = table.Column<DateTime>(type: "TEXT", nullable: false),
                    Content = table.Column<string>(type: "TEXT", nullable: true),
                    DiscordMessageModelId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DiscordEditMessageModel", x => x.DiscordEditMessageModelId);
                    table.ForeignKey(
                        name: "FK_DiscordEditMessageModel_DiscordMessageModels_DiscordMessageModelId",
                        column: x => x.DiscordMessageModelId,
                        principalTable: "DiscordMessageModels",
                        principalColumn: "DiscordMessageModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DiscordEditMessageModel_DiscordMessageModelId",
                table: "DiscordEditMessageModel",
                column: "DiscordMessageModelId");

            migrationBuilder.CreateIndex(
                name: "IX_DiscordMessageModels_MessageId",
                table: "DiscordMessageModels",
                column: "MessageId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DiscordEditMessageModel");

            migrationBuilder.DropTable(
                name: "DiscordMessageModels");
        }
    }
}
