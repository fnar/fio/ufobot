﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace UFOBot.Migrations
{
    /// <inheritdoc />
    public partial class RemoveDiscriminatorFromDatabase : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DiscordDiscriminator",
                table: "InGameUserToDiscordUserModels");

            migrationBuilder.DropColumn(
                name: "AuthorDiscriminator",
                table: "DiscordMessageModels");

            migrationBuilder.DropColumn(
                name: "DeletedByDiscriminator",
                table: "DiscordMessageModels");

            migrationBuilder.AddColumn<ulong>(
                name: "AuthorId",
                table: "DiscordMessageModels",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0ul);

            migrationBuilder.AddColumn<ulong>(
                name: "DeletedByUserId",
                table: "DiscordMessageModels",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0ul);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AuthorId",
                table: "DiscordMessageModels");

            migrationBuilder.DropColumn(
                name: "DeletedByUserId",
                table: "DiscordMessageModels");

            migrationBuilder.AddColumn<string>(
                name: "DiscordDiscriminator",
                table: "InGameUserToDiscordUserModels",
                type: "TEXT",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AuthorDiscriminator",
                table: "DiscordMessageModels",
                type: "TEXT",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DeletedByDiscriminator",
                table: "DiscordMessageModels",
                type: "TEXT",
                nullable: true);
        }
    }
}
