﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace UFOBot.Migrations
{
    public partial class AddIdToInGameUserToDiscordUserModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<ulong>(
                name: "DiscordId",
                table: "InGameUserToDiscordUserModels",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0ul);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DiscordId",
                table: "InGameUserToDiscordUserModels");
        }
    }
}
