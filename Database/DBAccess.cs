﻿using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore;
using System;
using UFOBot.Database;

namespace FIOAPI.DB
{
    /// <summary>
    /// A helper class to manage access to the DbContext
    /// </summary>
    public class DBAccess : IDisposable
    {
        private IDbContextTransaction Transaction = null;

        /// <summary>
        /// The FIODbContext
        /// </summary>
        public UFODbContext DB
        {
            get; private set;
        }

        /// <summary>
        /// Retrieves a reader to the DbContext
        /// </summary>
        /// <param name="TimeoutSeconds">Timeout</param>
        /// <returns>DBAccess object</returns>
        public static DBAccess GetReader(int? TimeoutSeconds = null)
        {
            return new DBAccess(false, TimeoutSeconds);
        }

        /// <summary>
        /// Retrieves a writer to the DbContext
        /// </summary>
        /// <param name="TimeoutSeconds">Timeout</param>
        /// <returns>DBAccess object</returns>
        public static DBAccess GetWriter(int? TimeoutSeconds = null)
        {
            return new DBAccess(true, TimeoutSeconds);
        }

        private DBAccess(bool ShouldCreateTransaction, int? TimeoutSeconds = null)
        {
            DB = new UFODbContext()
            {
                ShouldRestrictSaves = !ShouldCreateTransaction
            };

            if (TimeoutSeconds.HasValue)
            {
                DB.Database.SetCommandTimeout(TimeSpan.FromSeconds(TimeoutSeconds.Value));

            }
            if (ShouldCreateTransaction)
            {
                DB.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.TrackAll;
                Transaction = DB.Database.BeginTransaction();
            }
            else
            {
                DB.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
                DB.ChangeTracker.AutoDetectChangesEnabled = false;
            }
        }

        #region IDisposable Interface
        /// <summary>
        /// Dispose()
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose(bool)
        /// </summary>
        /// <param name="disposing">disposing</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                Transaction?.Commit();
                Transaction?.Dispose();
                Transaction = null;

                DB.Dispose();
            }
        }
        #endregion
    }
}
