﻿using System;

using Microsoft.EntityFrameworkCore.Storage;

namespace UFOBot.Database
{
    public class UFODbTransaction : IDisposable
    {
        public UFODbContext DB
        {
            get; private set;
        } = null;

        private IDbContextTransaction _transaction = null;

        public UFODbTransaction()
        {
            DB = new UFODbContext();
            _transaction = DB.Database.BeginTransaction();
        }

        public void Dispose()
        {
            _transaction?.Commit();
            _transaction?.Dispose();
            _transaction = null;

            DB?.Dispose();
            DB = null;
        }
    }
}
