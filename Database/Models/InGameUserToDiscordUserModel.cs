﻿using Newtonsoft.Json;

namespace UFOBot.Database.Models
{
	public class InGameUserToDiscordUserModel
	{
		[JsonIgnore]
		public int InGameUserToDiscordUserModelId { get; set; }

		public string InGameUsername { get; set; }

		public ulong DiscordId { get; set; }
		public string DiscordUsername { get; set; }
	}
}
