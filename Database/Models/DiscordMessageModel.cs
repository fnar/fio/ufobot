using System;
using System.Collections.Generic;

using DSharpPlus.Entities;
using Microsoft.EntityFrameworkCore;

namespace UFOBot.Database.Models
{
    [Index(nameof(MessageId))]
    public class DiscordMessageModel
    {
        public DiscordMessageModel()
        {

        }

        public DiscordMessageModel(DiscordMessage message)
        {
            MessageId = message.Id;
            ChannelId = message.Channel.Id;
            ChannelCategory = message.Channel.Parent?.Name;
            ChannelName = message.Channel.Name;
            AuthorId = message.Author.Id;
            AuthorUsername = message.Author.Username;
            TimestampUtc = message.Timestamp.UtcDateTime;
            EditedTimestampUtc = message.EditedTimestamp?.UtcDateTime;
            Content = message.Content;
            Deleted = false;
            DeletedTimestampUtc = null;
        }

        public int DiscordMessageModelId { get; set; }

        public ulong MessageId { get; set; }

        public ulong ChannelId { get; set; }
        public string ChannelCategory { get; set; }
        public string ChannelName { get; set; }

        public ulong AuthorId { get; set; }
        public string AuthorUsername { get; set; }

        public DateTime TimestampUtc { get; set; }
        public DateTime? EditedTimestampUtc { get; set; }

        public string Content { get; set; }

        public bool Deleted { get; set; }
        public ulong DeletedByUserId { get; set; }
        public string DeletedByUsername { get; set; }
        public DateTime? DeletedTimestampUtc { get; set; }

        public virtual List<DiscordEditMessageModel> Edits { get; set; } = new List<DiscordEditMessageModel>();
    }

    public class DiscordEditMessageModel
    {
        public DiscordEditMessageModel()
        {

        }

        public DiscordEditMessageModel(DiscordMessage msg)
        {
            TimestampUtc = DateTime.UtcNow;
            Content = msg.Content;
        }

        public int DiscordEditMessageModelId { get; set; }

        public DateTime TimestampUtc { get; set; }
        public string Content { get; set; }

        public int DiscordMessageModelId { get; set; }
        public virtual DiscordMessageModel DiscordMessageModel { get; set; }
    }
}