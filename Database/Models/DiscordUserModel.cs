﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using Microsoft.EntityFrameworkCore;

namespace UFOBot.Database.Models
{
	[Index(nameof(CompanyCode))]
	[Index(nameof(CorporationCode))]
	public class DiscordUserModel
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.None)]
		public ulong DiscordId { get; set; }

		public string DiscordUsername { get; set; }
		public string DiscordDiscriminator { get; set; }

		public bool MatchesConvention { get; set; }
		public bool Verified { get; set; }
		public string CorporationCode { get; set; }
		public string CompanyCode { get; set; }

		public DateTime NextMatchesConventionComplaint { get; set; }

		public void SetNoComplain()
		{
			NextMatchesConventionComplaint = DateTime.MaxValue;
		}

		public void SetNewComplain()
		{
			const double DaysBetweenBadNicknameComplaints = 1.0;
			DateTime now = DateTime.UtcNow;
			DateTime newComplainTime = now.AddDays(DaysBetweenBadNicknameComplaints);
			NextMatchesConventionComplaint = newComplainTime;
		}

		public bool ShouldComplain()
		{
			DateTime now = DateTime.UtcNow;
			return NextMatchesConventionComplaint < now;
		}
	}
}
