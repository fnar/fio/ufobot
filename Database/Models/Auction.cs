﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using Microsoft.EntityFrameworkCore;

namespace UFOBot.Database.Models
{
	public class Auction
	{
		[Key]
		[StringLength(64)]
		[DatabaseGenerated(DatabaseGeneratedOption.None)]
		public string AuctionId { get; set; } // {PlanetNaturalId}-{ChannelAuctionIndex} or {ChannelId-ChannelAuctionIndex} for the custom case

		public ulong ChannelId { get; set; }
		public string PlanetNaturalId { get; set; }

		public int ChannelAuctionIndex { get; set; }

		public DateTime CreationTime { get; set; }

		public DateTime BiddingEndTime { get; set; }
		public DateTime VerificationEndTime { get; set; }

		public bool AuctionCancelled { get; set; }

		[StringLength(100)]
		public string Summary { get; set; }

		[StringLength(1500)]
		public string Description { get; set; }

		public virtual List<HashedBid> HashedBids { get; set; } = new List<HashedBid>();
		public virtual List<UnhashedBid> UnhashedBids { get; set; } = new List<UnhashedBid>();


		[NotMapped]
		public bool BiddingActive
		{
			get
			{
				return DateTime.UtcNow < BiddingEndTime;
			}
		}

		[NotMapped]
		public bool VerificationActive
		{
			get
			{
				return !BiddingActive && DateTime.UtcNow < VerificationEndTime;
			}
		}

		[NotMapped]
		public bool AuctionActive
		{
			get
			{
				return BiddingActive || VerificationActive;
			}
		}

		[NotMapped]
		public bool AuctionHadNoBids
		{
			get
			{
				return HashedBids.Count == 0;
			}
		}

		[NotMapped]
		public bool AuctionHadNoVerifications
		{
			get
			{
				return UnhashedBids.Count == 0;
			}
		}
	}

	[Index(nameof(DiscordUserId))]
	[Index(nameof(InGameUserName))]
	public class HashedBid
	{
		public HashedBid()
		{

		}

		public HashedBid(Auction auction, string SHA256Hash, ulong DiscordUserId, string InGameUserName)
		{
			SHA256Hash = SHA256Hash.ToUpper();

			this.HashedBidId = $"{SHA256Hash}-{auction.AuctionId}";
			this.SHA256Hash = SHA256Hash;
			this.DiscordUserId = DiscordUserId;
			this.InGameUserName = InGameUserName;
			this.AuctionId = auction.AuctionId;
		}

		[Key]
		[StringLength(128)]
		[DatabaseGenerated(DatabaseGeneratedOption.None)]
		public string HashedBidId { get; set; } // {SHA256Hash}-{AuctionId}

		public string SHA256Hash { get; set; }

		public ulong DiscordUserId { get; set; }

		[StringLength(64)]
		public string InGameUserName { get; set; }

		[StringLength(64)]
		public string AuctionId { get; set; }

		public virtual Auction Auction { get; set; }
	}

	[Index(nameof(DiscordUserId))]
	[Index(nameof(InGameUserName))]
	public class UnhashedBid
	{
		public UnhashedBid()
		{

		}

		public UnhashedBid(Auction auction, string SHA256Hash, string UnhashedString, ulong DiscordUserId, string InGameUserName)
		{
			SHA256Hash = SHA256Hash.ToUpper();

			this.UnhashedBidId = $"{SHA256Hash}-{auction.AuctionId}";
			this.SHA256Hash = SHA256Hash;
			this.UnhashedString = UnhashedString;
			this.DiscordUserId = DiscordUserId;
			this.InGameUserName = InGameUserName;
			this.AuctionId = auction.AuctionId;
		}

		[Key]
		[StringLength(128)]
		[DatabaseGenerated(DatabaseGeneratedOption.None)]
		public string UnhashedBidId { get; set; } // {SHA256Hash}-{AuctionId}

		public string SHA256Hash { get; set; }

		[StringLength(350)]
		public string UnhashedString { get; set; }

		public ulong DiscordUserId { get; set; }

		[StringLength(64)]
		public string InGameUserName { get; set; }

		[StringLength(64)]
		public string AuctionId { get; set; }

		public virtual Auction Auction { get; set; }
	}
}
