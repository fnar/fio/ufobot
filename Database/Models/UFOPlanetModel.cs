﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using Microsoft.EntityFrameworkCore;

namespace UFOBot.Database.Models
{
	public class UFOPlanetModel
	{
		[Key]
		[StringLength(32)]
		[DatabaseGenerated(DatabaseGeneratedOption.None)]
		public string PlanetNaturalId { get; set; }

		public string PlanetName { get; set; }
	}
}
