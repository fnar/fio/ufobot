using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using UFOBot.Database.Models;

namespace UFOBot.Database
{
    public class UFODbContext : DbContext
    {
        public DbSet<DiscordMessageModel> DiscordMessageModels { get; set; }

        public DbSet<InGameUserToDiscordUserModel> InGameUserToDiscordUserModels { get; set; }

        public DbSet<DiscordUserModel> DiscordUsers { get; set; }

        public DbSet<Auction> Auctions { get; set; }
        public DbSet<HashedBid> HashedBids { get; set; }
        public DbSet<UnhashedBid> UnhashedBids { get; set; }

        public bool ShouldRestrictSaves { get; set; } = false;

        #region Override DbContext
        /// <summary>
        /// Overrides SaveChanges to enforce save restrictions
        /// </summary>
        /// <returns>Number of changes made</returns>
        public override int SaveChanges() => SaveChanges(true);

        /// <summary>
        /// Overrides SaveChanges to enforce save restrictions
        /// </summary>
        /// <param name="acceptAllChangesOnSuccess">acceptAllChangesOnSuccess</param>
        /// <returns>Number of changes made</returns>
        /// <exception cref="InvalidOperationException">If saves are not permitted</exception>
        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            if (ShouldRestrictSaves)
            {
                throw new InvalidOperationException("Saves not permitted");
            }

            return base.SaveChanges(acceptAllChangesOnSuccess);
        }

        /// <summary>
        /// Overrides SaveChangesAsync to enforce save restrictions
        /// </summary>
        /// <param name="cancellationToken">cancellationToken</param>
        /// <returns>Number of changes made</returns>
        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default) => SaveChangesAsync(true, cancellationToken);

        /// <summary>
        /// Overrides SaveChangesAsync to enforce save restrictions
        /// </summary>
        /// <param name="acceptAllChangesOnSuccess">acceptAllChangesOnSuccess</param>
        /// <param name="cancellationToken">cancellationToken</param>
        /// <returns>Number of changes made</returns>
        /// <exception cref="InvalidOperationException">If saves are not permitted</exception>
        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default)
        {
            if (ShouldRestrictSaves)
            {
                throw new InvalidOperationException("Saves not permitted");
            }

            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }
        #endregion

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseSqlite("Data Source=ufodb.db").UseLazyLoadingProxies();
        }
    }
}