﻿using System;
using System.Text.RegularExpressions;

namespace UFOBot.Utils
{
	public static class StringUtils
	{
		public static bool GetMessageLinkParts(this string str, out ulong CategoryId, out ulong ChannelId, out ulong MessageId)
		{
			CategoryId = 0;
			ChannelId = 0;
			MessageId = 0;

			string workingStr = str;

			const string DiscordMessagePrefix = "https://discord.com/channels/";
			if(workingStr.StartsWith(DiscordMessagePrefix))
			{
				workingStr = workingStr.Substring(DiscordMessagePrefix.Length);

				var messageLinkParts = workingStr.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
				if (ulong.TryParse(messageLinkParts[0], out CategoryId) && ulong.TryParse(messageLinkParts[1], out ChannelId) && ulong.TryParse(messageLinkParts[2], out MessageId))
				{
					return true;
				}
			}

			return false;
		}

		private const string SHA256HashPattern = @"^(?<SHA256Hash>[0-9a-fA-F]{32})$";
		private static Regex SHA256HashRegex = new Regex(SHA256HashPattern, RegexOptions.Compiled);
		public static bool IsSHA256Hash(this string input)
		{
			return SHA256HashRegex.IsMatch(input);
		}

		public static string GetUniveralTimeFormat(this DateTime dt)
		{
			return dt.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'");
		}
	}
}
