﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UFOBot.Utils
{
	public static class DateTimeUtils
	{
		public static long ToEpochSeconds(this DateTime date)
		{
			DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
			TimeSpan diff = date - epoch;
			return (long)Math.Floor(diff.TotalSeconds);
		}

		public static long ToEpochMilliseconds(this DateTime date)
		{
			DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
			TimeSpan diff = date - epoch;
			return (long)Math.Floor(diff.TotalMilliseconds);
		}
	}
}
