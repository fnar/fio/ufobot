using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace FIOWeb.Web
{
    public class Request
    {
        public static readonly HttpClient HttpClient;

        static Request()
        {
            HttpClient = new HttpClient();
            HttpClient.Timeout = TimeSpan.FromSeconds(300.0);
        }

        public HttpStatusCode StatusCode
        {
            get;
            private set;
        } = HttpStatusCode.UnavailableForLegalReasons;

        public string ResponsePayload
        {
            get;
            private set;
        }

        private HttpMethod Method = HttpMethod.Get;
        private string EndPoint = null;
        private string AuthToken = null;
        private string Payload = null;
        private string ContentType = null;

        public Request(HttpMethod Method, string EndPoint, string AuthToken = null, string Payload = null, string ContentType = null)
        {
            this.Method = Method;
            this.EndPoint = EndPoint;
            this.AuthToken = AuthToken;
            this.Payload = Payload;
            if (ContentType != null)
            {
                this.ContentType = ContentType;
            }
            else
            {
                this.ContentType = "application/json";
            }
        }

        public async Task<JsonRepr> GetResponseAsync<JsonRepr>()
        {
            return await GetResponseAsync<JsonRepr>(CancellationToken.None);
        }

        public async Task<JsonRepr> GetResponseAsync<JsonRepr>(CancellationToken ct)
        {
            StatusCode = HttpStatusCode.UnavailableForLegalReasons;
            ResponsePayload = null;

            try
            {
                using (HttpRequestMessage httpRequestMessage = new HttpRequestMessage(Method, WebConsts.RootUrl + EndPoint))
                {
                    //httpRequestMessage.Headers.AcceptEncoding.Add(new System.Net.Http.Headers.StringWithQualityHeaderValue("gzip"));

                    if (!String.IsNullOrWhiteSpace(AuthToken))
                    {
                        httpRequestMessage.Headers.Add("Authorization", AuthToken);
                    }
                    if (!String.IsNullOrWhiteSpace(Payload))
                    {
                        httpRequestMessage.Content = new StringContent(Payload, Encoding.UTF8, ContentType);
                    }

                    using (HttpResponseMessage response = await HttpClient.SendAsync(httpRequestMessage, HttpCompletionOption.ResponseContentRead, ct))
                    {
                        StatusCode = response.StatusCode;
                        ResponsePayload = await response.Content.ReadAsStringAsync(ct);
                        try
                        {
                            return JsonConvert.DeserializeObject<JsonRepr>(ResponsePayload);
                        }
                        catch
                        {
                            // @TODO: Log?
                        }
                    }
                }
            }
            catch (HttpRequestException e)
            {
                if (e.StatusCode != null)
                {
                    StatusCode = (HttpStatusCode)e.StatusCode;
                }
            }
            catch (TaskCanceledException)
            {
                StatusCode = HttpStatusCode.RequestTimeout;
                // @TODO: Log?
            }
            catch
            {

            }

            return default(JsonRepr);
        }

        public async Task<string> GetResultAsStringAsync()
        {
            return await GetResultAsStringAsync(CancellationToken.None);
        }

        public async Task<string> GetResultAsStringAsync(CancellationToken ct)
        {
            StatusCode = HttpStatusCode.UnavailableForLegalReasons;
            ResponsePayload = null;

            try
            {
                using (HttpRequestMessage httpRequestMessage = new HttpRequestMessage(Method, WebConsts.RootUrl + EndPoint))
                {
                    if (!String.IsNullOrWhiteSpace(AuthToken))
                    {
                        httpRequestMessage.Headers.Add("Authorization", AuthToken);
                    }
                    if (!String.IsNullOrWhiteSpace(Payload))
                    {
                        httpRequestMessage.Content = new StringContent(Payload, Encoding.UTF8, ContentType);
                    }

                    using (HttpResponseMessage response = await HttpClient.SendAsync(httpRequestMessage, HttpCompletionOption.ResponseContentRead, ct))
                    {
                        StatusCode = response.StatusCode;
                        if ( response.IsSuccessStatusCode )
                        {
                            ResponsePayload = await response.Content.ReadAsStringAsync(ct);
                        }
                    }
                }
            }
            catch
            {
                // @TODO: Log?
            }

            return ResponsePayload;
        }

        public async Task GetResultNoResponse()
        {
            await GetResultNoResponse(CancellationToken.None);
        }

        public async Task GetResultNoResponse(CancellationToken ct)
        {
            StatusCode = HttpStatusCode.UnavailableForLegalReasons;
            ResponsePayload = null;

            try
            {
                using (HttpRequestMessage httpRequestMessage = new HttpRequestMessage(Method, WebConsts.RootUrl + EndPoint))
                {
                    if (!String.IsNullOrWhiteSpace(AuthToken))
                    {
                        httpRequestMessage.Headers.Add("Authorization", AuthToken);
                    }
                    if (!String.IsNullOrWhiteSpace(Payload))
                    {
                        httpRequestMessage.Content = new StringContent(Payload, Encoding.UTF8, ContentType);
                    }

                    using (HttpResponseMessage response = await HttpClient.SendAsync(httpRequestMessage, HttpCompletionOption.ResponseContentRead, ct))
                    {
                        StatusCode = response.StatusCode;
                    }
                }
            }
            catch
            {
                // @TODO: Log?
            }
        }
    }    
}