﻿namespace UFOBot.DataAPI.FIO
{
	public class ChannelDef
	{
		public string DisplayName { get; set; }
		public string ChannelId { get; set; }
	}
}
