﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace UFOBot.DataAPI.FIO
{
	public class FIODataRetrieval : DataRetrieval
	{
		public static List<PlanetDef> CachedSettledPlanetDefs
		{
			get;
			private set;
		} = null;

		public override async Task<List<PlanetDef>> GetSettledPlanetDefs(bool ForceRetrieve = false)
		{
			if (ForceRetrieve || CachedSettledPlanetDefs == null )
			{
				var settledPlanetRequest = new FIOWeb.Web.Request(HttpMethod.Get, "/planet/allplanets/settled");
				CachedSettledPlanetDefs = await settledPlanetRequest.GetResponseAsync<List<PlanetDef>>();
				if (CachedSettledPlanetDefs == null || CachedSettledPlanetDefs.Count == 0)
				{
					CachedSettledPlanetDefs = null;
				}
			}

			return CachedSettledPlanetDefs;
		}

		public static List<MaterialDef> CachedMaterialDefs
		{
			get;
			private set;
		} = null;

		public override async Task<List<MaterialDef>> GetMaterialDefs(bool ForceRetrieve = false)
        {
			if (ForceRetrieve || CachedMaterialDefs == null)
			{
				var allMaterialsRequest = new FIOWeb.Web.Request(HttpMethod.Get, "/material/allmaterials");
				CachedMaterialDefs = await allMaterialsRequest.GetResponseAsync<List<MaterialDef>>();
				if (CachedMaterialDefs == null || CachedMaterialDefs.Count == 0)
				{
					CachedMaterialDefs = null;
				}
			}

			return CachedMaterialDefs;
		}

		private static string UFOChannelId = null;

		public override async Task<List<ChatMessageDef>> GetChatMessagesForUserInUFOChannel(string InGameUserName)
		{
			if (UFOChannelId == null)
			{
				const string UfoChatDisplayName = "UNITED FACTION OPERATIONS";
				var channelListRequest = new FIOWeb.Web.Request(HttpMethod.Get, "/chat/list");
				var channels = await channelListRequest.GetResponseAsync<List<ChannelDef>>();
				UFOChannelId = channels.Where(ch => ch.DisplayName.ToUpper() == UfoChatDisplayName).Select(ch => ch.DisplayName).FirstOrDefault();
				if (UFOChannelId == null)
				{
					return null;
				}
			}

			var chatMessagesRequest = new FIOWeb.Web.Request(HttpMethod.Get, $"/chat/user/{InGameUserName}/{UFOChannelId}");
			return await chatMessagesRequest.GetResponseAsync<List<ChatMessageDef>>();
		}
	}
}
