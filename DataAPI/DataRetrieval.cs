﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UFOBot.DataAPI
{
	public class DataRetrieval
	{
		public virtual async Task<List<PlanetDef>> GetSettledPlanetDefs(bool ForceRetrieve = false)
		{
			await Task.FromResult(0);
			return null;
		}

		public virtual async Task<List<ChatMessageDef>> GetChatMessagesForUserInUFOChannel(string InGameUserName)
		{
			await Task.FromResult(0);
			return null;
		}

		public virtual async Task<List<MaterialDef>> GetMaterialDefs(bool ForceRetrieve = false)
        {
			await Task.FromResult(0);
			return null;
        }
	}
}
