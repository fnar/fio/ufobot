﻿namespace UFOBot.DataAPI
{
    public class PlanetDef
    {
        public string Name { get; set; }
        public string NaturalId { get; set; }
        public int BaseCount { get; set; }
        public bool HasADM { get; set; }
        public bool HasLM { get; set; }
        public bool HasWarehouse { get; set; }
        public bool HasCOGC { get; set; }
        public string GovernorUsername { get; set; }
        public string GovernorCorporationName { get; set; }
        public string GovernorCorporationCode { get; set; }
    }
}
