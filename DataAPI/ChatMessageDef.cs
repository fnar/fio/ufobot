﻿using System;

namespace UFOBot.DataAPI
{
	public class ChatMessageDef
	{
		public string MessageId { get; set; }
		public string MessageType { get; set; }
		public string SenderId { get; set; }
		public string UserName { get; set; }
		public string MessageText { get; set; }
		public long MessageTimestamp { get; set; }
		public bool MessageDeleted { get; set; }
		public string UserNameSubmitted { get; set; }
		public DateTime Timestamp { get; set; }
		public string ChatModelId { get; set; }
	}
}
