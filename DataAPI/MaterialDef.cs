﻿namespace UFOBot.DataAPI
{
    public class MaterialDef
    {
        public string CategoryName { get; set; }
        public string CategoryId { get; set; }
        public string Name { get; set; }
        public string MatId { get; set; }
        public string Ticker { get; set; }
        public float Weight { get; set; }
        public float Volume { get; set; }
    }
}
